from django.urls import path

from . import views

app_name = 'appoinment'

urlpatterns = [
    path('buat/', views.buatappoinment, name='buatappoinment'),
    path('buat/<int:pk>', views.formappoinment, name='formappoinment'),
    path('data1/', views.dataappoinment1, name='dataappoinment1'),
    path('data1/upd/<int:pk>', views.formupdate, name='formupdate'),
    path('data1/del/<int:pk>', views.formdelete, name='formdelete'),

    # path('lihat/buat/', views.jadwaldokter_buat, name='jadwaldokter_buat'),
    # path('lihat/buat/<int:pk>', views.jadwaldokter_create, name='jadwaldokter_create'),


]
