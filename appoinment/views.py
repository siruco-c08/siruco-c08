from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect
from .forms import updateform


def buatappoinment(request):
    if(request.session['role']=='admin satgas' or request.session['role']=='Pengguna Publik'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.jadwal_dokter;")
        data_jadwal = fetch(cursor)

        return render(request, 'buatappoinment.html', {'data_jadwal':data_jadwal})
    else:
        return render(request, 'tolakappoinment.html')


def formappoinment(request,pk):
    cursor = connection.cursor()
    args={}
    message=""
    if request.method == 'POST':
        NIKPasien = request.POST.get('Kode')
        cursor = connection.cursor()
        # cursor.execute("SET search_path TO siruco;")
        cursor.execute("select * from SIRUCO.jadwal_dokter;")
        data = fetch(cursor)[pk]
        try:
            cursor.execute("INSERT INTO SIRUCO.MEMERIKSA(nik_pasien, nostr, username_dokter,kode_faskes, praktek_shift,praktek_tgl, rekomendasi)"
            "VALUES ('{}', '{}', '{}','{}', '{}', '{}', '{}');".format(NIKPasien,data['nostr'],data['username'],data['kode_faskes'],data['shift'],str(data['tanggal'])," "))
            print("hoyy")
            cursor.execute("UPDATE SIRUCO.jadwal_dokter SET jmlpasien=jmlpasien+1 WHERE NoSTR= '{}' AND Username='{}' AND Kode_Faskes='{}' AND Shift='{}' AND Tanggal='{}';"
            .format(data['nostr'],data['username'],data['kode_faskes'],data['shift'],str(data['tanggal'])," "))
            return redirect(reverse('appoinment:buatappoinment'))
        except Exception as e:
            print("Masuk")
            message=str(e)[0:29]
    
    if(request.session['role']=='Pengguna Publik'):
        cursor.execute("select NIK from SIRUCO.PASIEN WHERE idpendaftar='{}';".format(request.session['email']))
        dataNIK=fetch(cursor)
    else:
        cursor.execute("select NIK from SIRUCO.PASIEN;")
        dataNIK=fetch(cursor)
    args = {
        'data': dataNIK,
        'nomor':pk-1,
        'message':message
    }
    return render(request,'formappoinment.html',args)

def dataappoinment1(request):
    if(request.session['role']=='dokter'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.memeriksa WHERE username_dokter='{}';".format(request.session['email']))
        data_jadwal = fetch(cursor)
        return render(request, 'dataappoinment1.html', {'data_jadwal':data_jadwal})
    elif(request.session['role']=='admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.memeriksa;")
        data_jadwal = fetch(cursor)
        return render(request, 'dataappoinment2.html', {'data_jadwal':data_jadwal})
    elif(request.session['role']=='Pengguna Publik'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.memeriksa, SIRUCO.pasien where nik_pasien=nik AND idpendaftar='{}';"
        .format(request.session['email']))
        data_jadwal = fetch(cursor)
        return render(request, 'dataappoinment3.html', {'data_jadwal':data_jadwal})
    else:
        return render(request, 'tolakappoinment.html')
def formupdate(request,pk):
    cursor = connection.cursor()
    args={}
    message=""
    if request.method == 'POST':
        form = updateform(request.POST)
        if form.is_valid():
            rekomendasi = request.POST.get('rekomendasi')
            cursor = connection.cursor()
            # cursor.execute("SET search_path TO siruco;")
            cursor.execute("select * from SIRUCO.MEMERIKSA WHERE username_dokter='{}';".format(request.session['email']))
            data = fetch(cursor)[pk]
            # print(NIKPasien)
            # print(data)
            # print(str(data['tanggal']))
            try:
                cursor.execute("UPDATE SIRUCO.MEMERIKSA SET rekomendasi = '{}' WHERE NIK_Pasien= '{}' AND NoSTR = '{}' AND Username_dokter = '{}' AND Kode_Faskes='{}' AND praktek_shift= '{}' AND praktek_tgl='{}';"
                .format(rekomendasi,data['nik_pasien'], data['nostr'], data['username_dokter'], data['kode_faskes'], data['praktek_shift'], data['praktek_tgl']))
                return redirect(reverse('appoinment:dataappoinment1'))
            except Exception as e:
                message=str(e)[0:29]
    cursor.execute("select * from SIRUCO.MEMERIKSA WHERE username_dokter='{}';".format(request.session['email']))
    form = updateform()
    dataNIK=fetch(cursor)[pk-1]
    print(dataNIK)
    args = {
        'data': dataNIK,
        'nomor':pk-1,
        'message':message,
        'form' : form
    }
    return render(request,'formupdate.html',args)
def formdelete(request,pk):
    cursor = connection.cursor()
    args={}
    message=""
    if request.method == 'POST':    
        cursor = connection.cursor()
        # cursor.execute("SET search_path TO siruco;")
        cursor.execute("select * from SIRUCO.MEMERIKSA;")
        data = fetch(cursor)[pk]
        # print(NIKPasien)
        print(data)
        # print(str(data['tanggal']))
        try:
            cursor.execute("DELETE FROM SIRUCO.MEMERIKSA WHERE NIK_Pasien= '{}' AND NoSTR = '{}' AND Username_dokter = '{}' AND Kode_Faskes='{}' AND praktek_shift= '{}' AND praktek_tgl='{}';"
            .format(data['nik_pasien'], data['nostr'], data['username_dokter'], data['kode_faskes'], data['praktek_shift'], data['praktek_tgl']))
            return redirect(reverse('appoinment:dataappoinment1'))
        except Exception as e:
            message=str(e)[0:29]
    cursor.execute("select * from SIRUCO.MEMERIKSA;")
    dataNIK=fetch(cursor)[pk-1]
    args = {
        'data': dataNIK,
        'nomor':pk-1,
        'message':message,
    }
    return render(request,'formdelete.html',args)

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results
