from django.urls import path

from . import views

app_name = 'faskes'

urlpatterns = [
    path('', views.faskes_lihat, name='faskes_lihat'),
    path('buat/', views.faskes_buat, name='faskes_buat'),
    path('update/<str:kode>/', views.faskes_update, name='faskes_update'),
    path('detail/<str:kode>/', views.faskes_detail, name='faskes_detail'),
    path('hapus/<str:kode>/', views.faskes_hapus, name='faskes_hapus')
]
