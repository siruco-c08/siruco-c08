from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect, response
from .forms import *

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def faskes_lihat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.faskes")
        data_faskes = fetch(cursor)
        return render(request, 'faskes.html', {'data_faskes':data_faskes, 'role':role})

    else:
        return HttpResponseRedirect('/user/login')
	

def faskes_buat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'): 
        c = connection.cursor()
        c.execute("SELECT substring(kode,3) from SIRUCO.faskes order by substring(kode,3) desc limit 1")
        angka = c.fetchone()[0]
        angka = int (angka) + 1
        kode = "fs" + str(angka) 
        if request.method == 'POST':
            form = buatfaskes(request.POST)
            if form.is_valid():
                tipe = request.POST.get('tipe')
                nama = request.POST.get('nama')
                statusmilik = request.POST.get('statusmilik')
                jalan = request.POST.get('jalan')
                kelurahan = request.POST.get('kelurahan')
                kecamatan = request.POST.get('kecamatan')
                kabkot = request.POST.get('kabkot')
                prov = request.POST.get('prov')
                with connection.cursor() as c: 
                    c.execute("INSERT INTO SIRUCO.faskes VALUES ('{}','{}','{}','{}','{}','{}','{}','{}', '{}')".format(kode,tipe,nama,statusmilik,jalan,kelurahan,kecamatan,kabkot,prov))
            return HttpResponseRedirect(reverse('faskes:faskes_lihat'))
        form = buatfaskes()
        return render(request, 'buatfaskes.html', {'form':form,'kode':kode})

def faskes_detail(request, kode):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        with connection.cursor() as c:
            c.execute("SELECT * FROM SIRUCO.faskes WHERE kode = '{}'".format(kode))
            data = fetch(c)
        initial_data = {
            'kode' : data[0]['kode'],
            'tipe' : data[0]['tipe'],
            'nama' : data[0]['nama'],
            'statusmilik' : data[0]['statusmilik'],
            'jalan' : data [0]['jalan'],
            'kelurahan' : data [0]['kelurahan'],
            'kecamatan' : data [0]['kecamatan'],
            'kabkot' : data [0]['kabkot'],
            'prov' : data [0]['prov']
        }
        response = {'data': initial_data}
        return render(request,'detailfaskes.html', response)
    else:
        return HttpResponseRedirect(reverse('faskes:faskes_lihat'))

def faskes_update(request, kode):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        with connection.cursor() as c:
            c.execute("SELECT * FROM SIRUCO.faskes WHERE kode = '{}'".format(kode))
            data = fetch(c)
        initial_data = {
            'kode' : data[0]['kode'],
            'tipe' : data[0]['tipe'],
            'nama' : data[0]['nama'],
            'statusmilik' : data[0]['statusmilik'],
            'jalan' : data [0]['jalan'],
            'kelurahan' : data [0]['kelurahan'],
            'kecamatan' : data [0]['kecamatan'],
            'kabkot' : data [0]['kabkot'],
            'prov' : data [0]['prov'],
        }
        if request.method == 'POST':
            form = updatefaskes(request.POST)
            if form.is_valid():
                kode = initial_data['kode']
                tipe = request.POST.get('tipe')
                nama = request.POST.get('nama')
                statusmilik = request.POST.get('statusmilik')
                jalan = request.POST.get('jalan')
                kelurahan = request.POST.get('kelurahan')
                kecamatan = request.POST.get('kecamatan')
                kabkot = request.POST.get('kabkot')
                prov = request.POST.get('prov')
                
                c = connection.cursor()
                c.execute("UPDATE SIRUCO.faskes SET tipe = '{}', nama = '{}', statusmilik = '{}', jalan = '{}', kelurahan = '{}', kecamatan = '{}', kabkot = '{}', prov = '{}' WHERE kode = '{}';".format(tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov, kode))
                
                return HttpResponseRedirect(reverse('faskes:faskes_lihat'))

        update = updatefaskes(initial = initial_data)
        response = {'form':update, 'lama':initial_data}
        return render(request,'updatefaskes.html',response)
    else:
        return HttpResponseRedirect(reverse('faskes:faskes_lihat'))

def faskes_hapus(request, kode):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        with connection.cursor() as c:
            c.execute("delete from SIRUCO.faskes where kode= '{}'".format(kode))
        return HttpResponseRedirect(reverse('faskes:faskes_lihat'))
    else:
        return HttpResponseRedirect(reverse('faskes:faskes_lihat'))