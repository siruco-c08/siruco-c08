from django.urls import path

from . import views

app_name = 'hotel'

urlpatterns = [
    path('read/', views.hotel_read, name='hotel_read'),
    path('create/', views.hotel_create, name='hotel_create'),
    path('update/<str:kode>/', views.hotel_update, name='hotel_update'),
]
