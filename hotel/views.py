from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect, response
from .forms import *

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def hotel_read(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Admin' or role == 'Pengguna Publik' or role == 'admin satgas'):
        c = connection.cursor()
        c.execute("select * from SIRUCO.hotel")
        data_hotel = fetch(c)
        return render(request, 'hotel_list.html', {'data_hotel':data_hotel, 'role':role})
    else :
        return HttpResponseRedirect('/profil/profil')

def hotel_create(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Admin'):
        if (request.method == 'POST'):
            return HttpResponseRedirect(reverse('rumahsakit:rumahsakit_lihat'))
        return render(request, 'hotel_createhotel.html', {})
        c = connection.cursor()
        c.execute("SELECT substring(kode,3) from SIRUCO.hotel order by substring(kode,3) desc limit 1")
        angka = c.fetchone()[0]
        angka = int (angka) + 1
        kode = "H" + str(angka) 
        if request.method == 'POST':
            form = hotel_crt_form(request.POST)
            if form.is_valid():
                kode = request.POST.get('kode')
                nama = request.POST.get('nama')
                isrujukan = request.POST.get('Rujukan')
                jalan = request.POST.get('jalan')
                kelurahan = request.POST.get('kelurahan')
                kecamatan = request.POST.get('kecamatan')
                kabkot = request.POST.get('kabkot')
                prov = request.POST.get('prov')

                rujukanInt = 0
                if isrujukan == 'Rujukan':
                    rujukanInt = 1
                with connection.cursor() as c: 
                    c.execute("INSERT INTO SIRUCO.hotel VALUES ('{}','{}','{}','{}','{}','{}','{}','{}')".format(kode,nama,rujukanInt,jalan,kelurahan,kecamatan,kabkot,prov))
            return HttpResponseRedirect(reverse('hotel:hotel_read'))
        form = hotel_crt_form()
        return render(request, 'hotel_createhotel.html', {'form':form,'kode':kode})
    else:
        return HttpResponseRedirect(reverse('hotel:hotel_read'))


def hotel_update(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if role == 'Admin':
        with connection.cursor() as c:
            c.execute("SELECT * from SIRUCO.hotel where kode='{}'".format(kode))
            data=fetch(c)
        initial_data = {
            'kode' : data[0]['kode'],
            'nama' : data[0]['nama'],
            'isrujukan' : data[0]['isrujukan'],
            'jalan' : data [0]['jalan'],
            'kelurahan' : data [0]['kelurahan'],
            'kecamatan' : data [0]['kecamatan'],
            'kabkot' : data [0]['kabkot'],
            'prov' : data [0]['prov'],
        }
        if request.method == 'POST':
            form = hotel_upd_form(request.POST)
            if form.is_valid():
                kode = initial_data['kode']
                nama = request.POST.get('nama')
                isrujukan = request.POST.get('Rujukan')
                jalan = request.POST.get('jalan')
                kelurahan = request.POST.get('kelurahan')
                kecamatan = request.POST.get('kecamatan')
                kabkot = request.POST.get('kabkot')
                prov = request.POST.get('prov')

                rujukanInt = 0
                if isrujukan == 'Rujukan':
                    rujukanInt = 1
                
                c = connection.cursor()
                c.execute("UPDATE SIRUCO.hotel SET nama = '{}', isrujukan = '{}', jalan = '{}', kelurahan = '{}', kecamatan = '{}', kabkot = '{}', prov = '{}' WHERE kode = '{}';".format(nama, rujukanInt, jalan, kelurahan, kecamatan, kabkot, prov, kode))
                
                return HttpResponseRedirect(reverse('hotel:hotel_read'))

        update = hotel_upd_form(initial = initial_data)
        response = {'form':update, 'lama':initial_data}
        return render(request,'hotel_updatehotel.html',response)
    else:
        return HttpResponseRedirect(reverse('hotel:hotel_read'))
