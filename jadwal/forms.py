from django import forms
from django.db import connection
from django.core.paginator import Paginator

class buatjadwal(forms.Form):
    shift = forms.CharField(label='Shift', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    tanggal = forms.CharField(label='Tanggal', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})