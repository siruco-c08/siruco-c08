from django.urls import path

from . import views

app_name = 'jadwal'

urlpatterns = [
    path('', views.jadwal_lihat, name='jadwal_lihat'),
    path('buat/', views.jadwal_buat, name='jadwal_buat')
]
