from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect, response
from datetime import datetime
from .forms import *

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def jadwal_lihat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.jadwal")
        data_jadwal = fetch(cursor)
        return render(request, 'jadwal.html', {'data_jadwal':data_jadwal, 'role':role})

    else:
        return HttpResponseRedirect('/user/login')

def jadwal_buat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'): 
        c = connection.cursor()
        if request.method == 'POST':
            form = buatjadwal(request.POST)
            if form.is_valid():
                kode = request.POST.get('kode')
                shift = request.POST.get('shift')
                tanggal = request.POST.get('tanggal')
                tgl = datetime.strptime(tanggal, '%Y-%m-%d')
                with connection.cursor() as c:
                    print(request.POST)
                    print(kode)
                    c.execute("INSERT INTO SIRUCO.jadwal VALUES ('{}','{}','{}')".format(kode,shift,tgl))
            return HttpResponseRedirect(reverse('jadwal:jadwal_lihat'))
        c.execute("SELECT * from SIRUCO.faskes")
        data = fetch(c)
        form = buatjadwal()
        response = {'form':form, 'data':data}
        return render(request,'buatjadwalfaskes.html',response)
    else:
        return HttpResponseRedirect(reverse('jadwal:jadwal_lihat'))