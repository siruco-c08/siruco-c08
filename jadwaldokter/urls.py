from django.urls import path

from . import views

app_name = 'jadwaldokter'

urlpatterns = [
    path('lihat/', views.jadwaldokter_lihat, name='jadwaldokter_lihat'),
    path('lihat/buat/', views.jadwaldokter_buat, name='jadwaldokter_buat'),
    path('lihat/buat/<int:pk>', views.jadwaldokter_create, name='jadwaldokter_create'),


]
