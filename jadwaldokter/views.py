from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect


def jadwaldokter_lihat(request):
	if(request.session['role']=='dokter'):
		cursor = connection.cursor()
		cursor.execute("select * from SIRUCO.jadwal_dokter WHERE username='{}';".format(request.session['email']))
		data_jadwal = fetch(cursor)
		return render(request, 'jadwaldokter.html', {'data_jadwal':data_jadwal})
	elif(request.session['role']=='admin satgas' or request.session['role']=='Pengguna Publik'):
		cursor = connection.cursor()
		cursor.execute("select * from SIRUCO.jadwal_dokter;")
		data_jadwal = fetch(cursor)
		return render(request, 'jadwaldokter2.html', {'data_jadwal':data_jadwal})
	else:
		return render(request, 'tolakjadwal.html')


def jadwaldokter_buat(request):
	cursor = connection.cursor()
	cursor.execute("select * from SIRUCO.jadwal;")
	data_jadwal = fetch(cursor)
	# if request.method == 'POST':	
	# 	kodefaskes = request.POST.get('kode_faskes')
	# 	shift = request.POST.get('shift')
	# 	tanggal = request.POST.get('tanggal')
	# 	jmlpasien = request.POST.get('jmlpasien')
	# 	print(kodefaskes,shift,tanggal)
	# 	return redirect('/')

	return render(request, 'buatjadwal.html', {'data_jadwal':data_jadwal})

def jadwaldokter_create(request,pk):
	message=""
	cursor = connection.cursor()
	# cursor.execute("SET search_path TO siruco;")
	cursor.execute("select * from SIRUCO.jadwal;")
	data_jadwal = fetch(cursor)[pk-1]
	cursor.execute("SELECT * FROM siruco.dokter WHERE username='{}';".format(request.session['email']))
	data_dokter= fetch(cursor)[0]

	try:
		cursor.execute("INSERT INTO SIRUCO.jadwal_dokter(NoSTR, Username, Kode_Faskes,Shift, Tanggal,JmlPasien)"
		"VALUES ('{}', '{}', '{}', '{}', '{}', '{}');".format(data_dokter['nostr'],data_dokter['username'],data_jadwal['kode_faskes'], data_jadwal['shift'],data_jadwal['tanggal'],0))
	# if request.method == 'POST':	
	# 	kodefaskes = request.POST.get('kode_faskes')
	# 	shift = request.POST.get('shift')
	# 	tanggal = request.POST.get('tanggal')
	# 	jmlpasien = request.POST.get('jmlpasien')
	# 	print(kodefaskes,shift,tanggal)
	except Exception as e:
            print("waw")
            message=str(e)[0:29]	
	args={
        'message':message,
    }
	return render(request, 'confirm.html',args)





def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results
