from django.shortcuts import render,redirect
from django.contrib import messages
from django.urls import reverse


def home(request):
    if 'email' in request.session:
	    return redirect(reverse('profil:profil'))
    return render(request, 'main/home.html')
