from django import forms
from django.db import connection
from django.core.paginator import Paginator

class createreservasi(forms.Form):
    tglmasuk = forms.CharField(label='Tanggal Masuk', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    tglkeluar = forms.CharField(label='Tanggal Keluar', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})

class updatereservasi(forms.Form):
    tglkeluar = forms.CharField(label='Tanggal Keluar', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})

