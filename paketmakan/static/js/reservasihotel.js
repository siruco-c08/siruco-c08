//Ini fungsi buat ngatur default value
$(document).on({
  ajaxStart: function(){
      $("body").addClass("loading"); 
  },
  ajaxStop: function(){ 
      $("body").removeClass("loading"); 
  }    
});

$(document).ready(function () {
    var opsiroom = $('#koderoom');
    opsiroom.empty();
    //Default kode rs sama kode ruangan ini didapet dari view ya, jadi di view nanti kirim kode rs pertama sama kode ruangan where kode rsnya kode rs pertama itu
    var defaultkodehotel = 'ht1';
    
    $.ajax({
        url: '/reservasihotel/getkoderoom/',
        data: {
          'kodehotel': defaultkodehotel
        },
        dataType: 'json',
        success: function (data) {
            //yang console.log bisa dihapus aja, ini kalo buat debugging dan pgn tau data yg diterima wkwk
            console.log('success');
            console.log(defaultkodehotel);
            console.log(data);
            var hasil = data.dataRuangan;
            var opsikoderoom = $('#koderoom');
            opsikoderoom.empty();
            
            for (i = 0; i < hasil.length; i++) {
                console.log('masuk');
                var koderoom= hasil[i].koderoom;
                opsikoderoom.append("<option value='" + koderoom + "'>" + koderoom + "</option>");
            }      
        
        }
    });



//Ini fungsi buat ngatur pilihan kode ruangan ketika kode rs diubah
$('#kodehotel').change(function () {
  var kodehotel = $(this).val();

  $.ajax({
    url: '/reservasihotel/getkoderoom/',
    data: {
      'kodehotel': kodehotel
    },
    dataType: 'json',
    success: function (data) {
        console.log('success');
        console.log(data);
        var hasil = data.dataRuangan;
        var opsikoderoom = $('#koderoom');
        opsikoderoom.empty();
        
        for (i = 0; i < hasil.length; i++) {
            console.log('masuk');
            var koderoom= hasil[i].koderoom;
            opsikoderoom.append("<option value='" + koderoom + "'>" + koderoom + "</option>");
        }

        
    }
  });

});

});




