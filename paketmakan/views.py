from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from datetime import datetime
from django.contrib import messages
from django.http import JsonResponse 
from .forms import *


def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def read_reservasi_hotel(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from siruco.reservasi_hotel")
        data = fetch(cursor)
        for d in data :
            d['hapus'] = dapat_dihapus(d['kodepasien'],d['tglmasuk'])
        return render(request, 'listreservasi.html', {'data':data, 'role':role})
    if (role == 'Pengguna Publik'):
        cursor = connection.cursor()
        username = request.session['email']
        cursor.execute("select kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom from siruco.reservasi_hotel, siruco.pasien where kodepasien = nik and idpendaftar = '{}' ".format(username))
        data = fetch(cursor)
        return render(request, 'listreservasi.html', {'data':data, 'role':role })
    else:
        return HttpResponseRedirect('/profil/profil')
    


def create_reservasi_hotel(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        c = connection.cursor()
        c.execute("SELECT * from siruco.pasien")
        pasien = fetch(c)
        c.execute("SELECT * from siruco.hotel")
        hotel = fetch(c)
        defaultkodehotel = hotel[0]['kode']
        request.session['defaultkodehotel'] = defaultkodehotel;
        
        c.execute("SELECT * from siruco.hotel_room")
        room = fetch(c)
        if request.method == 'POST':
            form = createreservasi(request.POST)
            try: 
                if form.is_valid():
                    nik = request.POST.get('nik')
                    tglmasuk = request.POST.get('tglmasuk')
                    tglkeluar = request.POST.get('tglkeluar')
                    kodehotel = request.POST.get('kodehotel')
                    koderoom = request.POST.get('koderoom')
                    masuk = datetime.strptime(tglmasuk, '%Y-%m-%d')
                    keluar = datetime.strptime(tglkeluar, '%Y-%m-%d')
                    if (masuk < keluar):
                        with connection.cursor() as c:
                            c.execute("INSERT INTO SIRUCO.reservasi_hotel VALUES ('{}','{}','{}','{}','{}')".format(nik,tglmasuk,tglkeluar,kodehotel,koderoom))
                        return HttpResponseRedirect(reverse('reservasihotel:read_reservasi_hotel'))
                    else: 
                        messages.error(request, 'Tanggal masuk tidak boleh lebih besar dari tanggal keluar')
                        return redirect(reverse('reservasihotel:create_reservasi_hotel'))
            except:
                messages.error(request, 'Sudah ada reservasi dengan kode pasien dan tanggal yang sama!')
                return redirect(reverse('reservasihotel:create_reservasi_hotel'))
        form = createreservasi()
        response = {'form':form, 'pasien':pasien, 'hotel':hotel, 'room':room}
        response['defaultkodehotel'] = defaultkodehotel
        return render(request,'createreservasi.html',response)
    elif (role == 'Pengguna Publik'):
        c = connection.cursor()
        username = request.session['email']
        c.execute("SELECT * from siruco.pasien where idpendaftar = '{}' ".format(username))
        pasien = fetch(c)
        c.execute("SELECT * from siruco.hotel")
        hotel = fetch(c)
        c.execute("SELECT * from siruco.hotel_room")
        room = fetch(c)
        if request.method == 'POST':
            form = createreservasi(request.POST)
            try:
                if form.is_valid():
                    nik = request.POST.get('nik')
                    tglmasuk = request.POST.get('tglmasuk')
                    tglkeluar = request.POST.get('tglkeluar')
                    kodehotel = request.POST.get('kodehotel')
                    koderoom = request.POST.get('koderoom')
                    masuk = datetime.strptime(tglmasuk, '%Y-%m-%d')
                    keluar = datetime.strptime(tglkeluar, '%Y-%m-%d')
                    if (masuk < keluar):
                        with connection.cursor() as c:
                            c.execute("INSERT INTO SIRUCO.reservasi_hotel VALUES ('{}','{}','{}','{}','{}')".format(nik,tglmasuk,tglkeluar,kodehotel,koderoom))
                        return HttpResponseRedirect(reverse('reservasihotel:read_reservasi_hotel'))
                    else: 
                        messages.error(request, 'Tanggal masuk tidak boleh lebih besar dari tanggal keluar')
                        return redirect(reverse('reservasihotel:create_reservasi_hotel'))
            except:
                messages.error(request, 'Sudah ada reservasi dengan kode pasien dan tanggal yang sama!')
                return redirect(reverse('reservasihotel:create_reservasi_hotel'))
        form = createreservasi()
        response = {'form':form, 'pasien':pasien, 'hotel':hotel, 'room':room}
        return render(request,'createreservasi.html',response)
    else:
        return HttpResponseRedirect(reverse('reservasihotel:read_reservasi_hotel'))

def update_reservasi_hotel(request, kodepasien, tglmasuk):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        with connection.cursor() as c:
            c.execute("SELECT * FROM siruco.reservasi_hotel WHERE kodepasien = '{}' and tglmasuk = '{}' ".format(kodepasien, tglmasuk))
            data = fetch(c)
        initial_data = {
            'kodepasien' : data[0]['kodepasien'],
            'tglmasuk' : data[0]['tglmasuk'],
            'tglkeluar' : data[0]['tglkeluar'],
            'kodehotel' : data[0]['kodehotel'],
            'koderoom' : data [0]['koderoom']
        }
        if request.method == 'POST':
            form = updatereservasi(request.POST)
            if form.is_valid():
                tglkeluar = request.POST.get('tglkeluar')
                masuk = datetime.strptime(str(initial_data['tglmasuk']), '%Y-%m-%d')
                keluar = datetime.strptime(tglkeluar, '%Y-%m-%d')
                if (masuk < keluar):
                    c = connection.cursor()
                    c.execute("UPDATE siruco.reservasi_hotel SET tglkeluar = '{}' WHERE kodepasien= '{}' and tglmasuk='{}';".format(tglkeluar, kodepasien , tglmasuk))
                    return HttpResponseRedirect(reverse('reservasihotel:read_reservasi_hotel'))
                else: 
                    messages.error(request, 'Tanggal masuk tidak boleh lebih besar dari tanggal keluar')
                    return redirect('/reservasihotel/update/{}/{}'.format(kodepasien, tglmasuk))
        update = updatereservasi(initial = initial_data)
        response = {'form':update, 'lama':initial_data}
        return render(request,'updatereservasi.html',response)
    else:
        return HttpResponseRedirect(reverse('reservasihotel:read_reservasi_hotel'))


def dapat_dihapus(kodepasien, tglmasuk):
    c = connection.cursor()
    c.execute("select * from siruco.reservasi_hotel where tglmasuk > now() and kodepasien ='{}' and tglmasuk = '{}' ".format(kodepasien, tglmasuk))
    data = fetch(c)
    if (data != []):
        return True
    else:
        return False


def delete_reservasi(request, kodepasien, tglmasuk):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        if (dapat_dihapus(kodepasien,tglmasuk)):
            with connection.cursor() as c:
                c.execute("select idtransaksibooking from siruco.transaksi_booking where kodepasien = '{}' and tglmasuk = '{}' ".format(kodepasien,tglmasuk))
                data = fetch(c)[0]['idtransaksibooking']
                c.execute("delete from siruco.reservasi_hotel where kodepasien= '{}' and tglmasuk = '{}' ".format(kodepasien,tglmasuk))
                c.execute("delete from siruco.transaksi_hotel where idtransaksi = '{}' ".format(data))
            return HttpResponseRedirect(reverse('reservasihotel:read_reservasi_hotel'))
    else:
        return HttpResponseRedirect(reverse('reservasihotel:read_reservasi_hotel'))


def getkoderoom(request):
	kodehotel = request.GET.get('kodehotel', '')
	cursor = connection.cursor()
	cursor.execute("SELECT koderoom from siruco.hotel_room WHERE kodehotel='{}';".format(kodehotel))
	listKodeRuangan = fetch(cursor)
	return JsonResponse({'dataRuangan':listKodeRuangan})