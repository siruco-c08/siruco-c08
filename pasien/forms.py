from django import forms
from django.db import connection
from django.core.paginator import Paginator


class buatpasien(forms.Form):
    nik = forms.CharField(label='NIK', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    nama = forms.CharField(label='Nama', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    notelp = forms.CharField(label='No Telepon', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    nohp = forms.CharField(label='No HP', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_kelurahan = forms.CharField(label='Kelurahan',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_kecamatan = forms.CharField(label='Kecamatan',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_kabkot = forms.CharField(label='Kabupaten/Kota',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_prov = forms.CharField(label='Provinsi',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_kelurahan = forms.CharField(label='Kelurahan',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_kecamatan = forms.CharField(label='Kecamatan',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_kabkot = forms.CharField(label='Kabupaten/Kota',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_prov = forms.CharField(label='Provinsi',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})

class updatepasien(forms.Form):
    nik = forms.CharField(label='NIK', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    nama = forms.CharField(label='Nama', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    notelp = forms.CharField(label='No Telepon', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    nohp = forms.CharField(label='No HP', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_kelurahan = forms.CharField(label='Kelurahan',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_kecamatan = forms.CharField(label='Kecamatan',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_kabkot = forms.CharField(label='Kabupaten/Kota',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    ktp_prov = forms.CharField(label='Provinsi',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_kelurahan = forms.CharField(label='Kelurahan',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_kecamatan = forms.CharField(label='Kecamatan',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_kabkot = forms.CharField(label='Kabupaten/Kota',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    dom_prov = forms.CharField(label='Provinsi',  widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})