from django.urls import path

from . import views

app_name = 'pasien'

urlpatterns = [
    path('', views.pasien_lihat, name='pasien_lihat'),
    path('buat/', views.pasien_buat, name='pasien_buat'),
    path('update/<str:nik>/', views.pasien_update, name='pasien_update'),
    path('detail/<str:nik>/', views.pasien_detail, name='pasien_detail'),
    path('hapus/<str:nik>/', views.pasien_hapus, name='pasien_hapus')
]
