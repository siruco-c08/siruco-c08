from pasien.forms import buatpasien, updatepasien
from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect, response
# from .forms import *

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def pasien_lihat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Pengguna Publik'):
        idpendaftar = request.session['email']
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.pasien where idpendaftar = '{}'".format(idpendaftar))
        data_pasien = fetch(cursor)
        return render(request, 'pasien.html', {'data_pasien':data_pasien, 'role':role})

    else:
        return HttpResponseRedirect('/user/login')
	

def pasien_buat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Pengguna Publik'): 
        email = request.session['email'].split('@')
        idpendaftar = email[0]
        if request.method == 'POST':
            form = buatpasien(request.POST)
            if form.is_valid():
                nik = request.POST.get('nik')
                nama = request.POST.get('nama')
                notelp = request.POST.get('notelp')
                nohp = request.POST.get('nohp')
                ktp_jalan = request.POST.get('ktp_jalan')
                ktp_kelurahan = request.POST.get('ktp_kelurahan')
                ktp_kecamatan = request.POST.get('ktp_kecamatan')
                ktp_kabkot = request.POST.get('ktp_kabkot')
                ktp_prov = request.POST.get('ktp_prov')
                dom_jalan = request.POST.get('dom_jalan')
                dom_kelurahan = request.POST.get('dom_kelurahan')
                dom_kecamatan = request.POST.get('dom_kecamatan')
                dom_kabkot = request.POST.get('dom_kabkot')
                dom_prov = request.POST.get('dom_prov')
                with connection.cursor() as c: 
                    c.execute("INSERT INTO SIRUCO.pasien VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(nik, idpendaftar, nama, ktp_jalan,ktp_kelurahan,ktp_kecamatan,ktp_kabkot,ktp_prov,dom_jalan,dom_kelurahan,dom_kecamatan,dom_kabkot,dom_prov, notelp, nohp))
            return HttpResponseRedirect(reverse('pasien:pasien_lihat'))
        form = buatpasien()
        return render(request, 'buatpasien.html', {'form':form, 'data':idpendaftar})

def pasien_detail(request, nik):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Pengguna Publik'):
        with connection.cursor() as c:
            c.execute("SELECT * FROM SIRUCO.pasien WHERE nik = '{}'".format(nik))
            data = fetch(c)
            c.execute("SELECT * FROM SIRUCO.pengguna_publik B, SIRUCO.pasien P WHERE P.idpendaftar = B.username and P.nik = '{}'".format(nik))
            data2 = fetch(c)
        initial_data = {
            'nik' : data[0]['nik'],
            'idpendaftar' : data[0]['idpendaftar'],
            'nama' : data[0]['nama'],
            'ktp_jalan' : data[0]['ktp_jalan'],
            'ktp_kelurahan' : data [0]['ktp_kelurahan'],
            'ktp_kecamatan' : data [0]['ktp_kelurahan'],
            'ktp_kabkot' : data [0]['ktp_kabkot'],
            'ktp_prov' : data [0]['ktp_prov'],
            'dom_jalan' : data [0]['dom_jalan'],
            'dom_kelurahan' : data [0]['dom_kelurahan'],
            'dom_kecamatan' : data [0]['dom_kecamatan'],
            'dom_kabkot' : data [0]['dom_kabkot'],
            'dom_prov' : data [0]['dom_prov'],
            'notelp' : data [0]['notelp'],
            'nohp' : data [0]['nohp']
        }

        initial_data2 = {
            'username' : data2[0]['username']
        }
        response = {'data': initial_data, 'data2': initial_data2}
        return render(request,'detailpasien.html', response)
    else:
        return HttpResponseRedirect(reverse('pasien:pasien_lihat'))

def pasien_update(request, nik):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Pengguna Publik'):
        with connection.cursor() as c:
            c.execute("SELECT * FROM SIRUCO.pasien WHERE nik = '{}'".format(nik))
            data = fetch(c)
        initial_data = {
            'nik' : data[0]['nik'],
            'idpendaftar' : data[0]['idpendaftar'],
            'nama' : data[0]['nama'],
            'ktp_jalan' : data[0]['ktp_jalan'],
            'ktp_kelurahan' : data [0]['ktp_kelurahan'],
            'ktp_kecamatan' : data [0]['ktp_kelurahan'],
            'ktp_kabkot' : data [0]['ktp_kabkot'],
            'ktp_prov' : data [0]['ktp_prov'],
            'dom_jalan' : data [0]['dom_jalan'],
            'dom_kelurahan' : data [0]['dom_kelurahan'],
            'dom_kecamatan' : data [0]['dom_kecamatan'],
            'dom_kabkot' : data [0]['dom_kabkot'],
            'dom_prov' : data [0]['dom_prov'],
            'notelp' : data [0]['notelp'],
            'nohp' : data [0]['nohp']
        }
        if request.method == 'POST':
            form = updatepasien(request.POST)
            if form.is_valid():
                nik = request.POST.get('nik')
                nama = request.POST.get('nama')
                notelp = request.POST.get('notelp')
                nohp = request.POST.get('nohp')
                ktp_jalan = request.POST.get('ktp_jalan')
                ktp_kelurahan = request.POST.get('ktp_kelurahan')
                ktp_kecamatan = request.POST.get('ktp_kecamatan')
                ktp_kabkot = request.POST.get('ktp_kabkot')
                ktp_prov = request.POST.get('ktp_prov')
                dom_jalan = request.POST.get('dom_jalan')
                dom_kelurahan = request.POST.get('dom_kelurahan')
                dom_kecamatan = request.POST.get('dom_kecamatan')
                dom_kabkot = request.POST.get('dom_kabkot')
                dom_prov = request.POST.get('dom_prov')
                
                c = connection.cursor()
                c.execute("UPDATE SIRUCO.pasien SET nik = '{}', nama = '{}', notelp = '{}', nohp = '{}', ktp_jalan = '{}', ktp_kelurahan = '{}', ktp_kecamatan = '{}', ktp_kabkot = '{}', ktp_prov = '{}', dom_jalan = '{}', dom_kelurahan = '{}', dom_kecamatan = '{}', dom_kabkot = '{}', dom_prov = '{}' WHERE nik = '{}';".format(nik, nama, notelp, nohp, ktp_jalan, ktp_kelurahan, ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, dom_kecamatan, dom_kabkot, dom_prov, nik))
                
                return HttpResponseRedirect(reverse('pasien:pasien_lihat'))

        update = updatepasien(initial = initial_data)
        response = {'form':update, 'lama':initial_data}
        return render(request,'updatepasien.html',response)
    else:
        return HttpResponseRedirect(reverse('pasien:pasien_lihat'))

def pasien_hapus(request, nik):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Pengguna Publik'):
        with connection.cursor() as c:
            c.execute("delete from SIRUCO.pasien where nik= '{}'".format(nik))
        return HttpResponseRedirect(reverse('pasien:pasien_lihat'))
    else:
        return HttpResponseRedirect(reverse('pasien:pasien_lihat'))