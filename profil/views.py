from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse


def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]

def profil(request):
    if 'email' not in request.session:
        return HttpResponseRedirect(reverse('user:login'))
    
    email = request.session['email']
    role= request.session['role']
    cursor_p = connection.cursor()

    select = "SELECT * FROM siruco.akun_PENGGUNA WHERE username='{}';".format(email)
    cursor_p.execute(select)
    data = fetch(cursor_p)

    if role =='Admin':
        select="SELECT * FROM siruco.ADMIN WHERE username='"+email+"';"
        cursor_p.execute(select)
        data  = fetch(cursor_p)
        nama = email.split('@')[0]
        return render(request,'profiladmin.html',{'nama':nama})

    elif role == 'Pengguna Publik':
        select="SELECT * FROM siruco.pengguna_publik WHERE username='"+email+"';"
        cursor_p.execute(select)
        data= fetch(cursor_p)[0]
        return render(request,'profilpengguna.html',{'data':data})

    elif role == 'dokter':
        select="SELECT * FROM siruco.dokter WHERE username='"+email+"';"
        cursor_p.execute(select)
        data  = fetch(cursor_p)[0]
        return render(request,'profildokter.html',{'data':data})

    elif role == 'admin satgas':
        select= "SELECT * FROM siruco.admin_satgas WHERE username='"+email+"';"
        cursor_p.execute(select)
        data  = fetch(cursor_p)[0]
        nama = email.split('@')[0]
        return render(request,'profilsatgas.html',{'data':data, 'nama':nama})
