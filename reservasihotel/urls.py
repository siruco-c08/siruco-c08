from django.urls import path

from . import views

app_name = 'reservasihotel'

urlpatterns = [
    path('', views.read_reservasi_hotel, name='read_reservasi_hotel'),
    path('create', views.create_reservasi_hotel, name='create_reservasi_hotel'),
    path('update/<str:kodepasien>/<str:tglmasuk>', views.update_reservasi_hotel, name='update_reservasi_hotel'),
    path('delete/<str:kodepasien>/<str:tglmasuk>', views.delete_reservasi, name='delete_reservasi'),
    path('getkoderoom/',views.getkoderoom,name='getkoderoom'),
]
