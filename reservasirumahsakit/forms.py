from django import forms
from django.db import connection
from django.core.paginator import Paginator

class buatreservasirs(forms.Form):
    tglmasuk = forms.CharField(label='Shift', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    tglkeluar = forms.CharField(label='Tanggal', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})

class updatereservasirs(forms.Form):
    tglkeluar = forms.CharField(label='Tanggal', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})