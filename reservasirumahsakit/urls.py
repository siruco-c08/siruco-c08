from django.urls import path

from . import views

app_name = 'reservasirumahsakit'

urlpatterns = [
    path('', views.reservasirs_lihat, name='reservasirs_lihat'),
    path('buat/', views.reservasirs_buat, name='reservasirs_buat'),
    path('getkoderuangan/<str:koders>/',views.getKodeRuangan,name='getKodeRuangan'),
    path('getkodebed/<str:koders>/<str:koderuangan>/',views.getKodeBed,name='getKodeBed'),
    path('update/<str:kode>/<str:masuk>/', views.reservasirs_update, name='reservasirs_update'),
    path('delete/<str:kode>/<str:masuk>/', views.reservasirs_hapus, name='reservasirs_hapus'),
]
