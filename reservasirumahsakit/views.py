from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect, response, JsonResponse
from .forms import *

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def reservasirs_lihat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.reservasi_rs")
        data_reservasirs = fetch(cursor)
        for d in data_reservasirs :
            d['hapus'] = dapat_dihapus(d['kodepasien'],d['tglmasuk'])
        return render(request, 'reservasirs.html', {'data_reservasirs':data_reservasirs, 'role':role})

    elif (role == 'Pengguna Publik'):
        username = request.session['email']
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.reservasi_rs R, SIRUCO.Pasien P, SIRUCO.pengguna_publik B where R.KodePasien = P.nik and P.idPendaftar = B.username and B.username = '{}'".format(username))
        data_reservasirs = fetch(cursor)
        return render(request, 'reservasirs.html', {'data_reservasirs':data_reservasirs, 'role':role})

    else:
        return HttpResponseRedirect('/user/login')
	

def reservasirs_buat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'): 
        c = connection.cursor()
        if request.method == 'POST':
            form = buatreservasirs(request.POST)
            
            if form.is_valid():
                kodepasien = request.POST.get('kodepasien')
                tglmasuk = request.POST.get('tglmasuk')
                tglkeluar = request.POST.get('tglkeluar')
                koders = request.POST.get('koders')
                koderuangan = request.POST.get('koderuangan')
                kodebed = request.POST.get('kodebed')

                masuk = tglmasuk.split('-')
                keluar = tglkeluar.split('-')

                tahunm = int(masuk[0])
                tahunk = int(keluar[0])
                bulanm = int(masuk[1])
                bulank = int(keluar[1])
                tglm = int(masuk[2])
                tglk = int(keluar[2])

                if tahunm > tahunk:
                    messages.error(request, "Tanggal Keluar harus lebih besar dari Tanggal Masuk.")
                    return HttpResponseRedirect(reverse('reservasirumahsakit:reservasirs_buat'))
                
                if tahunm == tahunk and bulanm > bulank:
                    messages.error(request, "Tanggal Keluar harus lebih besar dari Tanggal Masuk.")
                    return HttpResponseRedirect(reverse('reservasirumahsakit:reservasirs_buat'))
                        
                if bulanm == bulank and tglm >= tglk:
                    messages.error(request, "Tanggal Keluar harus lebih besar dari Tanggal Masuk.")
                    return HttpResponseRedirect(reverse('reservasirumahsakit:reservasirs_buat'))

                with connection.cursor() as c: 
                    c.execute("INSERT INTO SIRUCO.reservasi_rs VALUES ('{}','{}','{}','{}','{}','{}')".format(kodepasien, tglmasuk, tglkeluar,koders, koderuangan, kodebed))
            return HttpResponseRedirect(reverse('reservasirumahsakit:reservasirs_lihat'))
        c.execute("SELECT nik from SIRUCO.pasien")
        pasien = fetch(c)
        c.execute("SELECT kode_faskes from SIRUCO.rumah_sakit")
        rumah_sakit = fetch(c)
        form = buatreservasirs()
        return render(request, 'buatreservasirs.html', {'form':form, 'pasien':pasien, 'rumah_sakit':rumah_sakit})

def getKodeRuangan(request, koders):
	cursor = connection.cursor()
	cursor.execute("SELECT KodeRuangan from SIRUCO.RUANGAN_RS WHERE kodeRS=%s;",[koders])
	listKodeRuangan = fetch(cursor)
	cursor.execute("SELECT KodeBed from SIRUCO.BED_RS WHERE kodeRS=%s and kodeRuangan=%s",[koders, listKodeRuangan[0]['koderuangan']])
	listKodeBed = fetch(cursor)
	return JsonResponse({'dataRuangan':listKodeRuangan,'dataBed':listKodeBed})

def getKodeBed(request, koders, koderuangan):
	cursor = connection.cursor()
	cursor.execute("SELECT KodeBed from SIRUCO.BED_RS WHERE kodeRS=%s and kodeRuangan=%s",[koders,koderuangan])
	listKodeBed = fetch(cursor)
	return JsonResponse({'data':listKodeBed})

def reservasirs_update(request, kode, masuk):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        with connection.cursor() as c:
            c.execute("SELECT * FROM SIRUCO.reservasi_rs WHERE kodepasien = '{}' and tglmasuk = '{}'".format(kode, masuk))
            data = fetch(c)
        initial_data = {
            'kodepasien' : data[0]['kodepasien'],
            'tglmasuk' : data[0]['tglmasuk'],
            'tglkeluar' : data[0]['tglkeluar'],
            'koders' : data[0]['koders'],
            'koderuangan' : data [0]['koderuangan'],
            'kodebed' : data [0]['kodebed'],
        }


        if request.method == 'POST':
            form = updatereservasirs(request.POST)
            if form.is_valid():
                tglkeluar = request.POST.get('tglkeluar')

                kodepasien = initial_data['kodepasien']
                masuk = initial_data['tglmasuk'].strftime("%Y-%m-%d").split('-')
                keluar = tglkeluar.split('-')

                tahunm = int(masuk[0])
                tahunk = int(keluar[0])
                bulanm = int(masuk[1])
                bulank = int(keluar[1])
                tglm = int(masuk[2])
                tglk = int(keluar[2])

                masuk1 = initial_data['tglmasuk'].strftime("%Y-%m-%d")

                if tahunm > tahunk:
                    messages.error(request, "Tanggal Keluar harus lebih besar dari Tanggal Masuk.")
                    return redirect('/reservasirs/update/{}/{}'.format(kodepasien, masuk1))
                
                if tahunm == tahunk and bulanm > bulank:
                    messages.error(request, "Tanggal Keluar harus lebih besar dari Tanggal Masuk.")
                    return redirect('/reservasirs/update/{}/{}'.format(kodepasien, masuk1))
                        
                if bulanm == bulank and tglm >= tglk:
                    messages.error(request, "Tanggal Keluar harus lebih besar dari Tanggal Masuk.")
                    return redirect('/reservasirs/update/{}/{}'.format(kodepasien, masuk1))
                
                c = connection.cursor()
                c.execute("UPDATE SIRUCO.reservasi_rs SET tglkeluar = '{}';".format(tglkeluar))
                
                return HttpResponseRedirect(reverse('reservasirumahsakit:reservasirs_lihat'))

        update = updatereservasirs(initial = initial_data)
        response = {'form':update, 'lama':initial_data}
        return render(request,'updatereservasirs.html',response)
    else:
        return HttpResponseRedirect(reverse('reservasirumahsakit:reservasirs_update'))

def dapat_dihapus(kodepasien, tglmasuk):
    c = connection.cursor()
    c.execute("select * from SIRUCO.reservasi_rs where tglmasuk > now() and kodepasien ='{}' and tglmasuk = '{}' ".format(kodepasien, tglmasuk))
    data = fetch(c)
    if (data != []):
        return True
    else:
        return False

def reservasirs_hapus(request, kode, masuk):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        if (dapat_dihapus(kode,masuk)):
            with connection.cursor() as c:
                c.execute("select idtransaksi from SIRUCO.transaksi_rs where kodepasien = '{}' and tglmasuk = '{}' ".format(kode,masuk))
                data = fetch(c)[0]['idtransaksi']
                c.execute("delete from SIRUCO.reservasi_rs where kodepasien= '{}' and tglmasuk = '{}' ".format(kode,masuk))
                c.execute("delete from SIRUCO.transaksi_rs where idtransaksi = '{}' ".format(data))
            return HttpResponseRedirect(reverse('reservasirumahsakit:reservasirs_lihat'))
    else:
        return HttpResponseRedirect(reverse('reservasirumahsakit:reservasirs_lihat'))