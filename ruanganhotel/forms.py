from django import forms
from django.db import connection
from django.core.paginator import Paginator

class createruangan(forms.Form):
    jenis = forms.CharField(label='Jenis Bed', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    tipe = forms.CharField(label='Tipe', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    harga = forms.IntegerField(label='Harga per hari',  widget=forms.NumberInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})

class updateruangan(forms.Form):
    jenisbed = forms.CharField(label='Jenis Bed', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    tipe = forms.CharField(label='Tipe', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})
    harga = forms.IntegerField(label='Harga per hari',  widget=forms.NumberInput(attrs={'class': 'form-control'}), required=True, error_messages = {'required': "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu"})