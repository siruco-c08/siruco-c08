from django.urls import path

from . import views

app_name = 'ruanghotel'

urlpatterns = [
    path('', views.read_ruangan_hotel, name='read_ruangan_hotel'),
    path('create', views.create_ruangan_hotel, name='create_ruangan_hotel'),
    path('update/<str:kodehotel>/<str:koderoom>', views.update_ruangan_hotel, name='update_ruangan_hotel'),
    path('delete/<str:kodehotel>/<str:koderoom>', views.delete_ruangan, name='delete_ruangan'),
]
