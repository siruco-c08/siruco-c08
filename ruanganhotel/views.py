from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *


def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def read_ruangan_hotel(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Admin' or role == 'Pengguna Publik' or role == 'admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from siruco.hotel_room")
        data = fetch(cursor)
        for d in data :
            d['hapus'] = dapat_dihapus(d['kodehotel'],d['koderoom'])
        return render(request, 'listruangan.html', {'data':data, 'role':role})



def create_ruangan_hotel(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Admin'): 
        c = connection.cursor()
        c.execute("SELECT siruco.get_max_id_hotel();")
        angka = c.fetchone()[0]
        angka = int (angka) + 1
        kodebaru = "hr" + str(angka) 
        if request.method == 'POST':
            form = createruangan(request.POST)

            if form.is_valid():
                kode = request.POST.get('Kode')
                tipe = request.POST.get('tipe')
                jenis = request.POST.get('jenis')
                harga = request.POST.get('harga')
                with connection.cursor() as c:
                    c.execute("INSERT INTO SIRUCO.Hotel_room VALUES ('{}','{}','{}','{}',{})".format(kode,kodebaru,jenis,tipe,harga))
            return HttpResponseRedirect(reverse('ruanghotel:read_ruangan_hotel'))
        c.execute("SELECT kode from SIRUCO.hotel")
        data = fetch(c)
        form = createruangan()
        response = {'form':form, 'data':data, 'kodebaru':kodebaru}
        return render(request,'createruangan.html',response)
    else:
        return HttpResponseRedirect(reverse('ruanghotel:read_ruangan_hotel'))

def update_ruangan_hotel(request, kodehotel, koderoom):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Admin'):
        with connection.cursor() as c:
            c.execute("SELECT * FROM siruco.hotel_room WHERE kodehotel = '{}' and koderoom = '{}' ".format(kodehotel,koderoom))
            data = fetch(c)
        initial_data = {
            'kodehotel' : data[0]['kodehotel'],
            'koderoom' : data[0]['koderoom'],
            'jenisbed' : data[0]['jenisbed'],
            'tipe' : data[0]['tipe'],
            'harga' : data [0]['harga']
        }
        if request.method == 'POST':
            form = updateruangan(request.POST)
            if form.is_valid():
                
                tipe = request.POST.get('tipe')
                jenisbed = request.POST.get('jenisbed')
                harga = request.POST.get('harga')
                kodehotel= initial_data['kodehotel']
                koderoom = initial_data['koderoom']
                c = connection.cursor()
                c.execute("UPDATE siruco.hotel_room SET tipe = '{}' WHERE kodehotel = '{}' and koderoom ='{}';".format(tipe, kodehotel,koderoom))
                c.execute("UPDATE siruco.hotel_room SET jenisbed = '{}' WHERE kodehotel = '{}' and koderoom ='{}';".format(jenisbed, kodehotel,koderoom))
                
                c.execute("UPDATE siruco.hotel_room SET harga = {} WHERE kodehotel = '{}' and koderoom ='{}';".format(harga, kodehotel,koderoom))
                
                return HttpResponseRedirect(reverse('ruanghotel:read_ruangan_hotel'))

        update = updateruangan(initial = initial_data)
        response = {'form':update, 'lama':initial_data}
        return render(request,'updateruangan.html',response)
    else:
        return HttpResponseRedirect(reverse('ruanghotel:read_ruangan_hotel'))


def dapat_dihapus(kodehotel, koderoom):
    c = connection.cursor()
    c.execute("select * from siruco.hotel_room r right outer join siruco.reservasi_hotel rs on r.kodehotel = rs.kodehotel and r.koderoom = rs.koderoom where rs.kodehotel = '{}' and rs.koderoom ='{}' ".format(kodehotel,koderoom))
    data = fetch(c)
    if (data == []):
        return True
    if (data != []):
        c.execute("select * from siruco.hotel_room r right outer join siruco.reservasi_hotel rs on r.kodehotel = rs.kodehotel and r.koderoom = rs.koderoom where rs.kodehotel = '{}' and rs.koderoom ='{}' and tglkeluar > now() ".format(kodehotel,koderoom))
        data = fetch(c)
        if (data == []):
            return True
    else:
        return False


def delete_ruangan(request, kodehotel, koderoom):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'Admin'):
        if (dapat_dihapus(kodehotel,koderoom)):
            with connection.cursor() as c:
                c.execute("delete from siruco.hotel_room where kodehotel= '{}' and koderoom = '{}' ".format(kodehotel,koderoom))
            return HttpResponseRedirect(reverse('ruanghotel:read_ruangan_hotel'))
    else:
        return HttpResponseRedirect(reverse('ruanghotel:read_ruangan_hotel'))