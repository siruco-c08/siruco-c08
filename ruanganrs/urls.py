from django.urls import path

from . import views

app_name = 'ruanganrs'

urlpatterns = [
    path('buat/', views.buatruangan, name='buatruangan'),
    path('buatbed/', views.buatbed, name='buatbed'),
    path('listruangan/', views.listruangan, name='listruangan'),
    path('listruangan/<int:pk>', views.formupdate, name='formupdate'),
    path('listruangan2/', views.listruangan2, name='listruangan2'),
    path('listruangan2/<int:pk>', views.formdelete, name='formdelete'),


    # path('buat/<int:pk>', views.formappoinment, name='formappoinment'),
    # path('data1/', views.dataappoinment1, name='dataappoinment1'),
    # path('data1/<int:pk>', views.formupdate, name='formupdate'),
    # path('data2/', views.dataappoinment2, name='dataappoinment2'),
    # path('data2/<int:pk>', views.formdelete, name='formdelete'),

    # path('lihat/buat/', views.jadwaldokter_buat, name='jadwaldokter_buat'),
    # path('lihat/buat/<int:pk>', views.jadwaldokter_create, name='jadwaldokter_create'),


]
