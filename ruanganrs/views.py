from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect
from .forms import formrs,updateform


def buatruangan(request):
    if(request.session['role']=='admin satgas'):
        cursor = connection.cursor()
        message={}
        if request.method == 'POST':
            form = formrs(request.POST)
            if form.is_valid():
                Koders = request.POST.get('Kode')
                tipe = request.POST.get('tipe')
                harga = request.POST.get('harga')
                cursor.execute("select koderuangan from SIRUCO.ruangan_rs where koders = '{}' order by koderuangan desc;".format(Koders))
                
                koderuangan= fetch(cursor)
                if(len(koderuangan)==0):
                    koderuangan="rg-1"
                else:
                    koderuangan=koderuangan[0]["koderuangan"][3:]
                    koderuangan="rg-"+str(int(str(koderuangan))+1)
                print(koderuangan)
                # print(NIKPasien)
                # print(data)
                # print(str(data['tanggal']))
                try:
                    cursor.execute("INSERT INTO SIRUCO.ruangan_rs(koders, koderuangan, tipe, jmlbed,harga)"
                    "VALUES ('{}', '{}', '{}', '{}', '{}');".format(Koders,koderuangan,tipe,0,harga))
                    return redirect('/')
                except Exception as e:
                    message=str(e)[0:29]
        cursor.execute("select kode_faskes from SIRUCO.rumah_sakit;")
        data_faskes = fetch(cursor)
        form = formrs()
        args={
            'data': data_faskes,
            'message':message,
            'form':form
        }
        return render(request, 'buatruangan.html', args)
    else:
        return render(request, 'tolak.html')

def buatbed(request):
    if(request.session['role']=='admin satgas'):
        cursor = connection.cursor()
        message={}
        if request.method == 'POST':
            kodegabungan = request.POST.get('Kode').split("--")
            koders = kodegabungan[0]
            koderuangan= kodegabungan[1]
            # print(koders)
            # print(koderuangan)
            cursor.execute("select kodebed from SIRUCO.bed_rs where koders = '{}' and koderuangan ='{}' order by kodebed desc;".format(koders,koderuangan))
            kodebed=fetch(cursor)
            print(kodebed)
            if(len(kodebed)==0):
                kodebed="bd-1"
            else:
                kodebed=kodebed[0]["kodebed"][3:]
                kodebed="bd-"+str(int(str(kodebed))+1)
            # print(kodebed)
            # koderuangan= fetch(cursor)
            # koderuangan=koderuangan[0]["koderuangan"][3:]
            # koderuangan="rg-"+str(int(str(koderuangan))+1)
            # print(NIKPasien)
            # print(data)
            # print(str(data['tanggal']))
            try:
                cursor.execute("INSERT INTO SIRUCO.bed_rs(koders, koderuangan, kodebed)"
                "VALUES ('{}', '{}', '{}');".format(koders,koderuangan,kodebed))
                return redirect('/')
            except Exception as e:
                print("waw")
                message=str(e)[0:29]
        cursor.execute("select koders, koderuangan from SIRUCO.ruangan_rs;")
        data_faskes = fetch(cursor)
        args={
            'data': data_faskes,
            'message':message,
        }
        return render(request, 'buatbed.html', args)
    else:
        return render(request, 'tolak.html')
def listruangan(request):
    if(request.session['role']=='admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.ruangan_rs;")
        data_ruangan = fetch(cursor)
        return render(request, 'listruanganrs.html', {'data_ruangan':data_ruangan})
    else:
        return render(request, 'tolak.html')


def formupdate(request,pk):
    cursor = connection.cursor()
    args={}
    message=""
    if request.method == 'POST':
        form = updateform(request.POST)
        if form.is_valid():
            tipe = request.POST.get('tipe')
            harga = request.POST.get('harga')

            cursor = connection.cursor()
            # cursor.execute("SET search_path TO siruco;")
            cursor.execute("select * from SIRUCO.ruangan_rs;")
            data = fetch(cursor)[pk]
            # print(NIKPasien)
            # print(data)
            # print(str(data['tanggal']))
            cursor.execute("UPDATE SIRUCO.ruangan_rs SET tipe ='{}', harga ='{}' WHERE koders='{}' AND koderuangan ='{}';"
            .format(tipe,int(harga), data['koders'], data['koderuangan']))
            return redirect(reverse('ruanganrs:listruangan'))
            # except Exception as e:
            #     print("Waw")
            #     message=str(e)[0:29]
    cursor.execute("select * from SIRUCO.ruangan_rs;")
    form = updateform()
    dataNIK=fetch(cursor)[pk-1]
    print(dataNIK)
    args = {
        'data': dataNIK,
        'message':message,
        'form' : form,
        'nomor':pk-1
    }
    return render(request,'formupdaters.html',args)

def listruangan2(request):
    if(request.session['role']=='admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.bed_rs;")
        data_bed = fetch(cursor)
        return render(request, 'listruanganrs2.html', {'data_bed':data_bed})
    else:
        return render(request, 'tolak.html')

def formdelete(request,pk):
    cursor = connection.cursor()
    args={}
    message=""
    cursor.execute("select * from SIRUCO.bed_rs;")
    data = fetch(cursor)[pk-1]
    cursor.execute("SELECT * FROM SIRUCO.BED_RS AS B WHERE KodeBed='{}' AND koderuangan='{}' AND koders='{}' AND KodeBed NOT IN(SELECT KodeBed FROM SIRUCO.RESERVASI_RS AS R WHERE  B.koders=r.koders AND B.kodebed= R.kodebed AND B.koderuangan=R.koderuangan);"
    .format(data['kodebed'],data['koderuangan'],data['koders']))
    databisa=fetch(cursor)
    if(len(databisa)==0): #bed tsb. sedang diresevasi
        cursor.execute("SELECT * FROM SIRUCO.RESERVASI_RS WHERE tglkeluar > DATE(CURRENT_TIMESTAMP) AND KodeBed='{}' AND koderuangan='{}' AND koders='{}';"
        .format(data['kodebed'],data['koderuangan'],data['koders']))
        tanggal=fetch(cursor)
        if(len(tanggal)==0): #tanggal keluar pasien < tanggal hari ini
            cursor.execute("DELETE FROM SIRUCO.BED_RS WHERE KodeBed='{}' AND koderuangan='{}' AND koders='{}';"
            .format(data['kodebed'],data['koderuangan'],data['koders']))
            print("Masuksini")
            message="Delete Berhasil"
        else:
            message="Bed sedang diresevasi dan pasien masih akan menggunakan Bed" 
    else:
        cursor.execute("DELETE FROM SIRUCO.BED_RS WHERE KodeBed='{}' AND koderuangan='{}' AND koders='{}';"
        .format(data["kodebed"],data["koderuangan"],data["koders"]))

        message="Delete Berhasil"
    args = {
        'message':message,
        'nomor':pk-1
    }
    return render(request,'formdeletebed.html',args)


def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results
