from django.urls import path

from . import views

app_name = 'rumahsakit'

urlpatterns = [
    path('lihat/', views.rumahsakit_lihat, name='rumahsakit_lihat'),
    path('buat/', views.rumahsakit_buat, name='rumahsakit_buat'),
    path('update/<str:kode>/', views.rumahsakit_update, name='rumahsakit_update')
]
