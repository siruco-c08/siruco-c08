from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect, response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def rumahsakit_lihat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.rumah_sakit")
        data_rumahsakit = fetch(cursor)
        return render(request, 'rumahsakit.html', {'data_rumahsakit':data_rumahsakit, 'role':role})

    else:
        return HttpResponseRedirect('/user/login')

def rumahsakit_buat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'): 
        c = connection.cursor()
        if request.method == 'POST':
            kode = request.POST.get('Kode')
            rujukan = request.POST.get('Rujukan')
            rujuk = 0
            if rujukan == 'Rujukan':
                rujuk = 1
            c.execute("INSERT INTO SIRUCO.rumah_sakit VALUES ('{}', {})".format(kode, rujuk))
            return HttpResponseRedirect(reverse('rumahsakit:rumahsakit_lihat'))
        c.execute("SELECT kode from SIRUCO.faskes except SELECT kode_faskes from SIRUCO.rumah_sakit;")
        data = fetch(c)
        response = {'data':data}
        return render(request,'buatrumahsakit.html',response)
    else:
        return HttpResponseRedirect(reverse('rumahsakit:rumahsakit_lihat'))

def rumahsakit_update(request, kode):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        with connection.cursor() as c:
            c.execute("SELECT * FROM SIRUCO.rumah_sakit WHERE kode_faskes = '{}'".format(kode))
            data = fetch(c)
        initial_data = {
            'kode' : data[0]['kode_faskes'],
        }
        if request.method == 'POST':
            kode = initial_data['kode']
            rujukan = request.POST.get('Rujukan')
            rujuk = 0
            if rujukan == 'Rujukan':
                rujuk = 1
                
            c = connection.cursor()
            c.execute("UPDATE SIRUCO.rumah_sakit SET isrujukan = {} where kode_faskes = '{}';".format(rujuk, kode))
                
            return HttpResponseRedirect(reverse('rumahsakit:rumahsakit_lihat'))

        response = {'lama':initial_data}
        return render(request,'updaterumahsakit.html',response)
    else:
        return HttpResponseRedirect(reverse('rumahsakit:rumahsakit_update'))

