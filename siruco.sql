--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.26
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: siruco; Type: SCHEMA; Schema: -; Owner: db202c08
--

CREATE SCHEMA siruco;


ALTER SCHEMA siruco OWNER TO db202c08;

--
-- Name: add_daftar_pesan(); Type: FUNCTION; Schema: siruco; Owner: db202c08
--

CREATE FUNCTION siruco.add_daftar_pesan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE
harga_paket INT;

BEGIN
harga_paket:= 0;

SELECT harga_paket + harga INTO harga_paket
FROM PAKET_MAKAN PM
WHERE NEW.KodeHotel = PM.KodeHotel AND NEW.KodePaket = PM.KodePaket;


IF(SELECT exists (
SELECT TM.IdTransaksiMakan 
FROM TRANSAKSI_MAKAN TM
WHERE TM.IdTransaksiMakan = NEW.IdTransaksiMakan) = true
)
THEN
UPDATE TRANSAKSI_MAKAN TM
SET TotalBayar = TotalBayar + harga_paket;
ELSE
INSERT INTO TRANSAKSI_MAKAN VALUES
(NEW.id_transaksi,NEW.IdTransaksiMakan,harga_paket);
END IF;

UPDATE TRANSAKSI_HOTEL TH
SET TotalBayar = TotalBayar + harga_paket;


RETURN NEW;
END
$$;


ALTER FUNCTION siruco.add_daftar_pesan() OWNER TO db202c08;

--
-- Name: add_transaksi_booking(); Type: FUNCTION; Schema: siruco; Owner: db202c08
--

CREATE FUNCTION siruco.add_transaksi_booking() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 DECLARE tanggalmasuk date;
 BEGIN
 SELECT tglmasuk into tanggalmasuk from reservasi_hotel r where r.kodepasien = NEW.kodepasien; 
 INSERT INTO TRANSAKSI_BOOKING
VALUES (NEW.idtransaksi, NEW.totalbayar, NEW.kodepasien, tanggalmasuk);
 RETURN NEW;
 END;
$$;


ALTER FUNCTION siruco.add_transaksi_booking() OWNER TO db202c08;

--
-- Name: add_transaksi_hotel(); Type: FUNCTION; Schema: siruco; Owner: db202c08
--

CREATE FUNCTION siruco.add_transaksi_hotel() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 DECLARE idtr varchar(5);
 DECLARE angkatr integer; 
 DECLARE totalharga integer;
 DECLARE totalhari integer;
 DECLARE hargaroom integer;
 BEGIN 
 SELECT substring(idtransaksi,4)::INTEGER
 into angkatr from transaksi_hotel order by substring(idtransaksi,4)::INTEGER desc limit 1;
 IF (angkatr IS NULL) then
  angkatr :=0;
 end if;
 angkatr := angkatr + 1;
 idtr := 'trh' || angkatr;
 SELECT abs(extract(day from CAST(tglmasuk AS timestamp) - CAST (tglkeluar AS timestamp))) into totalhari
 from reservasi_hotel r where NEW.Kodepasien = r.kodepasien;
 SELECT harga into hargaroom 
 from hotel_room hr where NEW.kodehotel = hr.kodehotel and NEW.koderoom = hr.koderoom;
 totalharga := totalhari * hargaroom; 
 INSERT INTO TRANSAKSI_HOTEL VALUES(idtr,NEW.kodepasien,NULL,NULL,totalharga,'Belum Lunas');
 RETURN NEW;
 END;
$$;


ALTER FUNCTION siruco.add_transaksi_hotel() OWNER TO db202c08;

--
-- Name: cek_jumlah_pasien(); Type: FUNCTION; Schema: siruco; Owner: db202c08
--

CREATE FUNCTION siruco.cek_jumlah_pasien() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF((SELECT JmlPasien FROM JADWAL_DOKTER as J WHERE NEW.Username_Dokter= J.Username 
AND J.NoSTR= NEW.NoSTR AND NEW.Praktek_tgl= J.Tanggal AND
NEW.Praktek_Shift= J.Shift AND NEW.Kode_Faskes = J.Kode_Faskes) >= 30)
THEN 
RAISE EXCEPTION 'Jumlah pasien sudah 30 atau lebih';
ELSE
UPDATE JADWAL_DOKTER as J SET JmlPasien = JmlPasien + 1 WHERE NEW.Username_Dokter= J.Username 
AND J.NoSTR= NEW.NoSTR AND NEW.Praktek_tgl= J.Tanggal AND
NEW.Praktek_Shift= J.Shift AND NEW.Kode_Faskes= J.Kode_Faskes;
END IF;
RETURN NEW;
END;
$$;


ALTER FUNCTION siruco.cek_jumlah_pasien() OWNER TO db202c08;

--
-- Name: cek_password(); Type: FUNCTION; Schema: siruco; Owner: db202c08
--

CREATE FUNCTION siruco.cek_password() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
Test varchar;
BEGIN
Test:=NEW.password;
IF NOT (Test ~ '.*[0-9].*' AND Test ~ '.*[A-Z].*') THEN
RAISE EXCEPTION 'Password anda tidak memenuhi syarat karena tidak mengandung 1 huruf kapital dan 1 angka';
END IF;
RETURN NEW;
END;
$$;


ALTER FUNCTION siruco.cek_password() OWNER TO db202c08;

--
-- Name: create_transaksi_rs(); Type: FUNCTION; Schema: siruco; Owner: db202c08
--

CREATE FUNCTION siruco.create_transaksi_rs() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    no_id int;
    kode_pasien varchar;
    tanggal_masuk date;
    total_hari int;
    total_harga int;
BEGIN
    SELECT substring(idTransaksi,5)::INTEGER
 into no_id from transaksi_rs order by substring(idTransaksi,5)::INTEGER desc limit 1; 
 no_id := no_id + 1;
    kode_pasien := NEW.KodePasien;
    tanggal_masuk := NEW.TglMasuk;


    SELECT abs(extract(day from CAST(tglmasuk AS timestamp) - CAST (tglkeluar AS timestamp))) into total_hari
 from reservasi_rs r where NEW.kodePasien = r.KodePasien and NEW.tglmasuk = r.tglmasuk;

    total_harga := 500000*total_hari;

    INSERT INTO TRANSAKSI_RS VALUES
        (CONCAT('trs-',no_id), kode_pasien, NULL, NULL, tanggal_masuk, total_harga, 'Belum Lunas');
 RETURN NEW;
END;
$$;


ALTER FUNCTION siruco.create_transaksi_rs() OWNER TO db202c08;

--
-- Name: nambah_bed(); Type: FUNCTION; Schema: siruco; Owner: db202c08
--

CREATE FUNCTION siruco.nambah_bed() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
UPDATE RUANGAN_RS as R SET JmlBed = JmlBed + 1 WHERE 
New.KodeRS= R.KodeRS AND NEW.KodeRuangan= R.KodeRuangan;
RETURN NEW;
ELSEIF (TG_OP = 'DELETE') THEN
UPDATE RUANGAN_RS as R SET JmlBed = JmlBed - 1 WHERE 
OLD.KodeRS= R.KodeRS AND OLD.KodeRuangan= R.KodeRuangan;
RETURN OLD;
END IF;
END;
$$;


ALTER FUNCTION siruco.nambah_bed() OWNER TO db202c08;

--
-- Name: pengurangan_bed(); Type: FUNCTION; Schema: siruco; Owner: db202c08
--

CREATE FUNCTION siruco.pengurangan_bed() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
UPDATE RUANGAN_RS as R SET JmlBed = JmlBed - 1 WHERE 
New.KodeRS= R.KodeRS AND NEW.KodeRuangan= R.KodeRuangan;
RETURN NEW;
ELSEIF (TG_OP = 'DELETE') THEN
UPDATE RUANGAN_RS as R SET JmlBed = JmlBed + 1 WHERE 
OLD.KodeRS= R.KodeRS AND OLD.KodeRuangan= R.KodeRuangan;
RETURN OLD;
END IF;
END;
$$;


ALTER FUNCTION siruco.pengurangan_bed() OWNER TO db202c08;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.admin (
    username character varying(50) NOT NULL
);


ALTER TABLE siruco.admin OWNER TO db202c08;

--
-- Name: admin_satgas; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.admin_satgas (
    username character varying(50) NOT NULL,
    idfaskes character varying(3)
);


ALTER TABLE siruco.admin_satgas OWNER TO db202c08;

--
-- Name: akun_pengguna; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.akun_pengguna (
    username character varying(50) NOT NULL,
    password character varying(20) NOT NULL,
    peran character varying(20) NOT NULL
);


ALTER TABLE siruco.akun_pengguna OWNER TO db202c08;

--
-- Name: bed_rs; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.bed_rs (
    koderuangan character varying(5) NOT NULL,
    koders character varying(3) NOT NULL,
    kodebed character varying(5) NOT NULL
);


ALTER TABLE siruco.bed_rs OWNER TO db202c08;

--
-- Name: daftar_pesan; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.daftar_pesan (
    idtransaksimakan character varying(10) NOT NULL,
    id_pesanan integer NOT NULL,
    id_transaksi character varying(10) NOT NULL,
    kodehotel character varying(5) NOT NULL,
    kodepaket character varying(5) NOT NULL
);


ALTER TABLE siruco.daftar_pesan OWNER TO db202c08;

--
-- Name: daftar_pesan_id_pesanan_seq; Type: SEQUENCE; Schema: siruco; Owner: db202c08
--

CREATE SEQUENCE siruco.daftar_pesan_id_pesanan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE siruco.daftar_pesan_id_pesanan_seq OWNER TO db202c08;

--
-- Name: daftar_pesan_id_pesanan_seq; Type: SEQUENCE OWNED BY; Schema: siruco; Owner: db202c08
--

ALTER SEQUENCE siruco.daftar_pesan_id_pesanan_seq OWNED BY siruco.daftar_pesan.id_pesanan;


--
-- Name: dokter; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.dokter (
    username character varying(50) NOT NULL,
    nostr character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    nohp character varying(12) NOT NULL,
    gelardepan character varying(10) NOT NULL,
    gelarbelakang character varying(10) NOT NULL
);


ALTER TABLE siruco.dokter OWNER TO db202c08;

--
-- Name: faskes; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.faskes (
    kode character varying(3) NOT NULL,
    tipe character varying(30) NOT NULL,
    nama character varying(50) NOT NULL,
    statusmilik character varying(30) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL
);


ALTER TABLE siruco.faskes OWNER TO db202c08;

--
-- Name: gedung; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.gedung (
    kodegedung character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL
);


ALTER TABLE siruco.gedung OWNER TO db202c08;

--
-- Name: gejala_pasien; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.gejala_pasien (
    nik character varying(20) NOT NULL,
    namagejala character varying(50) NOT NULL
);


ALTER TABLE siruco.gejala_pasien OWNER TO db202c08;

--
-- Name: hotel; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.hotel (
    kode character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL
);


ALTER TABLE siruco.hotel OWNER TO db202c08;

--
-- Name: hotel_room; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.hotel_room (
    kodehotel character varying(5) NOT NULL,
    koderoom character varying(5) NOT NULL,
    jenisbed character varying(10) NOT NULL,
    tipe character varying(10) NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.hotel_room OWNER TO db202c08;

--
-- Name: jadwal; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.jadwal (
    kode_faskes character varying(3) NOT NULL,
    shift character varying(15) NOT NULL,
    tanggal date NOT NULL
);


ALTER TABLE siruco.jadwal OWNER TO db202c08;

--
-- Name: jadwal_dokter; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.jadwal_dokter (
    nostr character varying(20) NOT NULL,
    username character varying(50) NOT NULL,
    kode_faskes character varying(3) NOT NULL,
    shift character varying(15) NOT NULL,
    tanggal date NOT NULL,
    jmlpasien integer
);


ALTER TABLE siruco.jadwal_dokter OWNER TO db202c08;

--
-- Name: kamar_rumah; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.kamar_rumah (
    koderumah character varying(5) NOT NULL,
    kodekamar character varying(5) NOT NULL,
    jenisbed character varying(20) NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.kamar_rumah OWNER TO db202c08;

--
-- Name: komorbid_pasien; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.komorbid_pasien (
    nik character varying(20) NOT NULL,
    namakomorbid character varying(50) NOT NULL
);


ALTER TABLE siruco.komorbid_pasien OWNER TO db202c08;

--
-- Name: memeriksa; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.memeriksa (
    nik_pasien character varying(20) NOT NULL,
    nostr character varying(20) NOT NULL,
    username_dokter character varying(50) NOT NULL,
    kode_faskes character varying(3) NOT NULL,
    praktek_shift character varying(15) NOT NULL,
    praktek_tgl date NOT NULL,
    rekomendasi character varying(500)
);


ALTER TABLE siruco.memeriksa OWNER TO db202c08;

--
-- Name: paket_makan; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.paket_makan (
    kodehotel character varying(5) NOT NULL,
    kodepaket character varying(5) NOT NULL,
    nama character varying(20) NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.paket_makan OWNER TO db202c08;

--
-- Name: pasien; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.pasien (
    nik character varying(20) NOT NULL,
    idpendaftar character varying(50),
    nama character varying(50) NOT NULL,
    ktp_jalan character varying(30) NOT NULL,
    ktp_kelurahan character varying(30) NOT NULL,
    ktp_kecamatan character varying(30) NOT NULL,
    ktp_kabkot character varying(30) NOT NULL,
    ktp_prov character varying(30) NOT NULL,
    dom_jalan character varying(30) NOT NULL,
    dom_kelurahan character varying(30) NOT NULL,
    dom_kecamatan character varying(30) NOT NULL,
    dom_kabkot character varying(30) NOT NULL,
    dom_prov character varying(30) NOT NULL,
    notelp character varying(20) NOT NULL,
    nohp character varying(12) NOT NULL
);


ALTER TABLE siruco.pasien OWNER TO db202c08;

--
-- Name: pengguna_publik; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.pengguna_publik (
    username character varying(50) NOT NULL,
    nik character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    status character varying(10) NOT NULL,
    peran character varying(20) NOT NULL,
    nohp character varying(12) NOT NULL
);


ALTER TABLE siruco.pengguna_publik OWNER TO db202c08;

--
-- Name: reservasi_gedung; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.reservasi_gedung (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    kodegedung character varying(5) NOT NULL,
    noruang character varying(5) NOT NULL,
    notempattidur character varying(5) NOT NULL
);


ALTER TABLE siruco.reservasi_gedung OWNER TO db202c08;

--
-- Name: reservasi_hotel; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.reservasi_hotel (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    kodehotel character varying(5),
    koderoom character varying(5)
);


ALTER TABLE siruco.reservasi_hotel OWNER TO db202c08;

--
-- Name: reservasi_rs; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.reservasi_rs (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date NOT NULL,
    koders character varying(3),
    koderuangan character varying(5),
    kodebed character varying(5),
    kodeventilator character varying(5)
);


ALTER TABLE siruco.reservasi_rs OWNER TO db202c08;

--
-- Name: reservasi_rumah; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.reservasi_rumah (
    kodepasien character varying(20) NOT NULL,
    tglmasuk date NOT NULL,
    tglkeluar date,
    koderumah character varying(5) NOT NULL,
    kodekamar character varying(5) NOT NULL
);


ALTER TABLE siruco.reservasi_rumah OWNER TO db202c08;

--
-- Name: ruangan_gedung; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.ruangan_gedung (
    kodegedung character varying(5) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    namaruangan character varying(20) NOT NULL
);


ALTER TABLE siruco.ruangan_gedung OWNER TO db202c08;

--
-- Name: ruangan_rs; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.ruangan_rs (
    koders character varying(3) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    tipe character varying(10) NOT NULL,
    jmlbed integer NOT NULL,
    harga integer NOT NULL
);


ALTER TABLE siruco.ruangan_rs OWNER TO db202c08;

--
-- Name: rumah; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.rumah (
    koderumah character varying(5) NOT NULL,
    nama character varying(30) NOT NULL,
    isrujukan character(1) NOT NULL,
    jalan character varying(30) NOT NULL,
    kelurahan character varying(30) NOT NULL,
    kecamatan character varying(30) NOT NULL,
    kabkot character varying(30) NOT NULL,
    prov character varying(30) NOT NULL
);


ALTER TABLE siruco.rumah OWNER TO db202c08;

--
-- Name: rumah_sakit; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.rumah_sakit (
    kode_faskes character varying(3) NOT NULL,
    isrujukan character(1) NOT NULL
);


ALTER TABLE siruco.rumah_sakit OWNER TO db202c08;

--
-- Name: telepon_faskes; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.telepon_faskes (
    kode_faskes character varying(3) NOT NULL,
    notelp character varying(20) NOT NULL
);


ALTER TABLE siruco.telepon_faskes OWNER TO db202c08;

--
-- Name: tempat_tidur_gedung; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.tempat_tidur_gedung (
    kodegedung character varying(5) NOT NULL,
    koderuangan character varying(5) NOT NULL,
    notempattidur character varying(5) NOT NULL
);


ALTER TABLE siruco.tempat_tidur_gedung OWNER TO db202c08;

--
-- Name: tes; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.tes (
    nik_pasien character varying(20) NOT NULL,
    tanggaltes date NOT NULL,
    jenis character varying(10) NOT NULL,
    status character varying(15) NOT NULL,
    nilaict character varying(5)
);


ALTER TABLE siruco.tes OWNER TO db202c08;

--
-- Name: transaksi_booking; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.transaksi_booking (
    idtransaksibooking character varying(10) NOT NULL,
    totalbayar integer,
    kodepasien character varying(20),
    tglmasuk date
);


ALTER TABLE siruco.transaksi_booking OWNER TO db202c08;

--
-- Name: transaksi_hotel; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.transaksi_hotel (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20),
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    totalbayar integer,
    statusbayar character varying(15) NOT NULL
);


ALTER TABLE siruco.transaksi_hotel OWNER TO db202c08;

--
-- Name: transaksi_makan; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.transaksi_makan (
    idtransaksi character varying(10) NOT NULL,
    idtransaksimakan character varying(10) NOT NULL,
    totalbayar integer
);


ALTER TABLE siruco.transaksi_makan OWNER TO db202c08;

--
-- Name: transaksi_rs; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.transaksi_rs (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20),
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    tglmasuk date,
    totalbiaya integer,
    statusbayar character varying(15) NOT NULL
);


ALTER TABLE siruco.transaksi_rs OWNER TO db202c08;

--
-- Name: transaksi_rumah; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.transaksi_rumah (
    idtransaksi character varying(10) NOT NULL,
    kodepasien character varying(20) NOT NULL,
    tanggalpembayaran date,
    waktupembayaran timestamp without time zone,
    totalbayar integer,
    statusbayar character varying(15) NOT NULL,
    tglmasuk date NOT NULL
);


ALTER TABLE siruco.transaksi_rumah OWNER TO db202c08;

--
-- Name: ventilator; Type: TABLE; Schema: siruco; Owner: db202c08
--

CREATE TABLE siruco.ventilator (
    koders character varying(3) NOT NULL,
    kodeventilator character varying(5) NOT NULL,
    kondisi character varying(10) NOT NULL
);


ALTER TABLE siruco.ventilator OWNER TO db202c08;

--
-- Name: daftar_pesan id_pesanan; Type: DEFAULT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.daftar_pesan ALTER COLUMN id_pesanan SET DEFAULT nextval('siruco.daftar_pesan_id_pesanan_seq'::regclass);


--
-- Data for Name: admin; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.admin (username) FROM stdin;
tphinnessy0
cthurstan1
pmcmyler2
rbuckwell3
djauncey4
vwalcot5
jrimmer6
ghowroyd7
jlennon8
mbruneton9
phatchera
drolesb
ajannyc
tswailed
smamwelle
rayllettf
acrabg
ckrollmannm
cshirrelln
sbedwello
\.


--
-- Data for Name: admin_satgas; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.admin_satgas (username, idfaskes) FROM stdin;
tphinnessy0	fs1
cthurstan1	fs1
pmcmyler2	fs2
rbuckwell3	fs2
djauncey4	fs3
vwalcot5	fs3
jrimmer6	fs4
ghowroyd7	fs4
jlennon8	fs5
mbruneton9	fs5
\.


--
-- Data for Name: akun_pengguna; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.akun_pengguna (username, password, peran) FROM stdin;
tphinnessy0	42Mm4OEWU1n	Admin
cthurstan1	pd2b9tPb	Admin
pmcmyler2	oshxs0h	Admin
rbuckwell3	MKKzLa	Admin
djauncey4	AYgdMG5LEK	Admin
vwalcot5	eV0tt4Rk	Admin
jrimmer6	ZcjWkKyb7gw	Admin
ghowroyd7	P8yJ797TXsn	Admin
jlennon8	ikv1fy5	Admin
mbruneton9	3cSEuUwi98	Admin
phatchera	bXN9fwbUlR4	Admin
drolesb	cVrTb1jgVKw	Admin
ajannyc	oPydUsEPjgl	Admin
tswailed	v6EuD96	Admin
smamwelle	uCrGGyW41U	Admin
rayllettf	6UdvLoGMp	Admin
acrabg	PV0ujVwnt	Admin
spatemanh	QHBfx9j	Pengguna Publik
bvanni	LXwxJ32L	Pengguna Publik
kmarkiej	f53RoHovB	Pengguna Publik
mshurmerk	Jka07xdgZ	Pengguna Publik
hjazel	lfe9ok5Af	Pengguna Publik
ckrollmannm	YCOdzHtf	Admin
cshirrelln	DKPxA1x	Admin
sbedwello	w1hQWiSe	Admin
\.


--
-- Data for Name: bed_rs; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.bed_rs (koderuangan, koders, kodebed) FROM stdin;
rg-01	fs1	bd-01
rg-01	fs1	bd-02
rg-01	fs1	bd-03
rg-01	fs1	bd-04
rg-02	fs1	bd-05
rg-03	fs2	bd-06
rg-03	fs2	bd-07
rg-04	fs2	bd-08
rg-04	fs2	bd-09
rg-04	fs2	bd-10
rg-04	fs2	bd-11
rg-05	fs3	bd-12
rg-05	fs3	bd-13
rg-05	fs3	bd-14
rg-05	fs3	bd-15
rg-05	fs3	bd-16
rg-05	fs3	bd-17
rg-05	fs3	bd-18
rg-05	fs3	bd-19
rg-05	fs3	bd-20
rg-06	fs3	bd-21
rg-06	fs3	bd-22
rg-07	fs4	bd-23
rg-07	fs4	bd-24
rg-07	fs4	bd-25
rg-07	fs4	bd-26
rg-08	fs4	bd-27
rg-08	fs4	bd-28
rg-08	fs4	bd-29
rg-08	fs4	bd-30
rg-08	fs4	bd-31
rg-08	fs4	bd-32
rg-09	fs5	bd-33
rg-09	fs5	bd-34
rg-10	fs5	bd-35
\.


--
-- Data for Name: daftar_pesan; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.daftar_pesan (idtransaksimakan, id_pesanan, id_transaksi, kodehotel, kodepaket) FROM stdin;
trm1	1	trh1	ht2	pm3
trm2	2	trh2	ht1	pm1
trm3	3	trh3	ht3	pm5
trm4	4	trh4	ht3	pm5
trm5	5	trh5	ht5	pm10
trm4	6	trh4	ht3	pm5
trm2	7	trh2	ht1	pm2
\.


--
-- Data for Name: dokter; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.dokter (username, nostr, nama, nohp, gelardepan, gelarbelakang) FROM stdin;
tphinnessy0	7311101006	Andika hesi	8121212121	Prof.	Sp.
cthurstan1	7312341122	Tristan cisko	8121212122	Prof.	Sp.
pmcmyler2	7312343321	Cyler adam	8121212123	Prof.	Sp.
rbuckwell3	7312341111	Faraz buckwell	8121212124	Prof.	S.ked
djauncey4	7312345644	Jon ecey	8121212125	Prof.	S.ked
\.


--
-- Data for Name: faskes; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.faskes (kode, tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
fs1	Rumah sakit	RS. Cendekia	Pemerintah	Jl. Ahmad Yani	Kelurahan Kalisari	Cakung Barat	Jakarta Timur	DKI Jakarta
fs2	Puskesmas	Puskesmas Polim	Pemerintah	Jl. Gajah Mada	Kelurahan Pekayon	Cakung Timur	Jakarta Barat	DKI Jakarta
fs3	Rumah sakit	RS. Gatot Kaca	Perseorangan	Jl. Jaksa	Kelurahan Cijantung	Jatinegara	Jakarta Timur	DKI Jakarta
fs4	Rumah sakit	RS. Oradio	Pemerintah	Jl. Gajah Mada	Kelurahan Baru	Penggilingan	Jakarta Pusat	DKI Jakarta
fs5	Rumah sakit	RS. Sudirman	Perusahaan	Jl. Jaksa	Kelurahan Gedong 	Pulo Gebang	Jakarta Utama	DKI Jakarta
\.


--
-- Data for Name: gedung; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.gedung (kodegedung, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
ged1	Gedung Breaking Bad	0	Jl. Durian	Durian Montong	Durian Runtuh	Jakarta Timur	DKI Jakarta
ged2	Gedung Vincenzo	1	Jl. Corn Salad	Babel	Consigliere	Cikaki	Jawa Barat
ged3	Gedung Start-Up	1	Jl. Nam	Han	Yeong-Sil	Jakarta Selatan	DKI Jakarta
ged4	Gedung La Casa de Papel	1	Jl. Bernoulli	Picasso	Pinnero	Jakarta Pusat	DKI Jakarta
ged5	Gedung Peaky Blinders	0	Jl. Thomas	London Jaya	Inggris Lawas	Cicakdidinding	Jawa Barat
ged6	Gedung Vagabond	0	Jl. Dal Guun	Samael	Suzy Simp	Jakarta Barat	DKI Jakarta
ged7	Gedung Lucifer	1	Jl. Amenadiel	San Jose	Carpe Diem	Jakarta Selatan	DKI Jakarta
ged8	Gedung Del Luna	1	Jl. Mahatma	Sailendra	Arjuna	Pandawa	Jawa Barat
\.


--
-- Data for Name: gejala_pasien; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.gejala_pasien (nik, namagejala) FROM stdin;
124776612	Meriang
564032689	Batuk berdahak
7408764615	Pilek
1700594451	Muntaber
9752161014	Sinus
6581815322	Cotangen
2237116377	Nyeri
670931411	Pegel linu
6111245511	Diare
9335827525	Migrain
\.


--
-- Data for Name: hotel; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.hotel (kode, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
ht1	Hotel Ibas	1	Jl. Kramat Raya	Kwitang	Senen	Jakarta Pusat	DKI Jakarta
ht2	Hotel Plaza	1	Jl. Tol S. Parman	Kota Bambu Utara	Palmerah	Jakarta Barat	DKI Jakarta
ht3	Hotel Genderang	0	Jl. Matraman Raya	Palmeriam	Matraman	Jakarta Timur	DKI Jakarta
ht4	Hotel Kurero	0	Jl. Bangka Raya	Pela Mampang	Mampang Prapatan	Jakarta Selatan	DKI Jakarta
ht5	Hotel Moonight	0	Jl. Danau Permai Raya	Sunter Jaya	Sunter	Jakarta Utara	DKI Jakarta
\.


--
-- Data for Name: hotel_room; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.hotel_room (kodehotel, koderoom, jenisbed, tipe, harga) FROM stdin;
ht1	hr1	Single Bed	Standard	150000
ht1	hr2	Single Bed	Standard	150000
ht1	hr3	Twin Bed	Standard	250000
ht2	hr4	Single Bed	Standard	150000
ht2	hr5	Twin Bed	Standard	250000
ht2	hr6	Twin Bed	Deluxe	400000
ht3	hr7	Single Bed	Deluxe	450000
ht3	hr8	Twin Bed	Standard	400000
ht3	hr9	Twin Bed	Deluxe	500000
ht4	hr10	Single Bed	Deluxe	450000
ht4	hr11	Twin Bed	Standard	450000
ht4	hr12	Double Bed	Deluxe	550000
ht5	hr13	Single Bed	Suite	900000
ht5	hr14	Twin Bed	Deluxe	700000
ht5	hr15	Double Bed	Standard	700000
\.


--
-- Data for Name: jadwal; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.jadwal (kode_faskes, shift, tanggal) FROM stdin;
fs3	11:00 - 15:00	2019-02-02
fs1	09:00 - 13:00	2019-06-10
fs1	09:00 - 16:00	2019-07-10
fs4	11:00 - 17:00	2019-08-13
fs5	09:00 - 11:00	2019-08-06
fs2	15:00 - 21:00	2019-09-07
fs3	16:00 - 21:00	2019-09-08
fs4	07:00 - 11:00	2019-10-17
fs5	11:00 - 21:00	2019-10-12
\.


--
-- Data for Name: jadwal_dokter; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.jadwal_dokter (nostr, username, kode_faskes, shift, tanggal, jmlpasien) FROM stdin;
7311101006	tphinnessy0	fs1	09:00 - 16:00	2019-07-10	1
7312341122	cthurstan1	fs4	11:00 - 17:00	2019-08-13	1
7312343321	pmcmyler2	fs5	09:00 - 11:00	2019-08-06	2
7312341111	rbuckwell3	fs2	15:00 - 21:00	2019-09-07	2
7312345644	djauncey4	fs3	16:00 - 21:00	2019-09-08	2
7311101006	tphinnessy0	fs4	07:00 - 11:00	2019-10-17	1
7312343321	pmcmyler2	fs5	11:00 - 21:00	2019-10-12	1
\.


--
-- Data for Name: kamar_rumah; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.kamar_rumah (koderumah, kodekamar, jenisbed, harga) FROM stdin;
rum1	kam1	single	600000
rum1	kam2	double	900000
rum2	kam1	single	600000
rum2	kam2	double	900000
rum3	kam1	single	600000
rum3	kam2	double	900000
rum4	kam1	single	600000
rum4	kam2	double	900000
rum5	kam1	single	600000
rum5	kam2	double	900000
rum6	kam1	single	600000
rum6	kam2	double	900000
\.


--
-- Data for Name: komorbid_pasien; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.komorbid_pasien (nik, namakomorbid) FROM stdin;
124776612	Gundala
564032689	Arjuna
7408764615	GatotKaca
1700594451	Minotaur
9752161014	Badang
6581815322	Kadita
2237116377	Rafaela
670931411	Angela
6111245511	Paqito
9335827525	Chou
\.


--
-- Data for Name: memeriksa; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.memeriksa (nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl, rekomendasi) FROM stdin;
124776612	7312343321	pmcmyler2	fs5	09:00 - 11:00	2019-08-06	Pada pemeriksaan kesehatan yang sudah dilakukan pada hari ini maka dengan ini menyatakan yang bersangkutan dalam kondisi tidak sehat,, sehingga perlu beristirahat selama 5 hari, dari tanggal 16 - 20 Mei 2018. Untuk itu, saudara dengan nama diatas tidak  bisa bekerja sebagaimana biasanya. Demikian surat keterangan ini ddibuat dan diberikan untuk diketahui dan dipergunakan sebagaimana mestinya.
564032689	7312341111	rbuckwell3	fs2	15:00 - 21:00	2019-09-07	\N
7408764615	7312345644	djauncey4	fs3	16:00 - 21:00	2019-09-08	\N
1700594451	7311101006	tphinnessy0	fs1	09:00 - 16:00	2019-07-10	Berdasar hasil pemeriksaan yang telah dilakukan, pasien tersebut dalam keadaan sakit, sehingga perlu beristirahat selama 3 hari, dari tanggal 16 - 18 Mei 2018. Diagnosa : Demam Berdarah. Demikian surat keterangan ini diberikan, untuk diketahui dan dipergunakan sebagaimana mestinya
9752161014	7312341122	cthurstan1	fs4	11:00 - 17:00	2019-08-13	\N
6581815322	7312345644	djauncey4	fs3	16:00 - 21:00	2019-09-08	\N
2237116377	7311101006	tphinnessy0	fs4	07:00 - 11:00	2019-10-17	Pada pemeriksaan yang bersangkutan dalam keadaan sakit, sehingga perlu istirahat selama 3 (tiga) hari dari tanggal 16 - 18 Mei 2018. Demikianlah surat keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya. Malang, 16 Mei 2018.
670931411	7312343321	pmcmyler2	fs5	11:00 - 21:00	2019-10-12	\N
6111245511	7312343321	pmcmyler2	fs5	09:00 - 11:00	2019-08-06	\N
9335827525	7312341111	rbuckwell3	fs2	15:00 - 21:00	2019-09-07	Berhubung dengan sakitnya perlu diberi istirahat selama 1 hari pada tanggal 16 Mei 2018. Sehubungan dengan hal tersebut, maka diharapkan agar pihak kampus dari Universitas Sumatera Utara bisa memberikan izin kepada yang bersangkutan. Medan, 16 Mei 2018.
\.


--
-- Data for Name: paket_makan; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.paket_makan (kodehotel, kodepaket, nama, harga) FROM stdin;
ht1	pm1	Paket Makan Besar	35000
ht1	pm2	Paket Dessert	20000
ht2	pm3	Paket Makan Besar	35000
ht2	pm4	Paket Dessert	20000
ht3	pm5	Paket Makan Besar	70000
ht3	pm6	Paket Dessert	50000
ht4	pm7	Paket Makan Besar	80000
ht4	pm8	Paket Dessert	60000
ht5	pm9	Paket Makan Besar	90000
ht5	pm10	Paket Dessert	65000
\.


--
-- Data for Name: pasien; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.pasien (nik, idpendaftar, nama, ktp_jalan, ktp_kelurahan, ktp_kecamatan, ktp_kabkot, ktp_prov, dom_jalan, dom_kelurahan, dom_kecamatan, dom_kabkot, dom_prov, notelp, nohp) FROM stdin;
124776612	spatemanh	Amory Dimock	Buena Vista	Kelurahan Kalisari	Cakung Barat	Jakarta Timur	DKI Jakarta	Buena Vista	Cakung Barat	Kelurahan Kalisari	Jakarta Timur	DKI Jakarta	569-226-5220	682-819-4452
564032689	bvanni	Cristine Bew	Carioca	Kelurahan Pekayon	Cakung Timur	Jakarta Barat	DKI Jakarta	Carioca	Cakung Timur	Kelurahan Pekayon	Jakarta Barat	DKI Jakarta	531-359-3706	718-952-9971
7408764615	kmarkiej	Terrence Lindop	Moose	Kelurahan Cijantung	Jatinegara	Jakarta Timur	DKI Jakarta	Moose	Jatinegara	Kelurahan Cijantung	Jakarta Timur	DKI Jakarta	177-360-8829	611-107-2101
1700594451	mshurmerk	Sidonnie Romera	Morrow	Kelurahan Baru	Penggilingan	Jakarta Pusat	DKI Jakarta	Morrow	Penggilingan	Kelurahan Baru	Jakarta Pusat	DKI Jakarta	812-621-6802	207-804-5128
9752161014	hjazel	Dyanne Kingstne	Golf	Kelurahan Gedong 	Pulo Gebang	Jakarta Utama	DKI Jakarta	Golf	Pulo Gebang	Kelurahan Gedong 	Jakarta Utama	DKI Jakarta	988-704-3650	875-670-4956
6581815322	spatemanh	Olin Ollerenshaw	Dovetail	Kelurahan Cibubur	Rawa Terate	Jakarta Selatan	DKI Jakarta	Dovetail	Rawa Terate	Kelurahan Cibubur	Jakarta Selatan	DKI Jakarta	214-540-0738	629-445-4382
2237116377	bvanni	Bart Partridge	Blue Bill Park	Kelurahan Kelapa Dua Wetan	Ujung Menteng	Jakarta Utara	DKI Jakarta	Blue Bill Park	Ujung Menteng	Kelurahan Kelapa Dua Wetan	Jakarta Utara	DKI Jakarta	838-762-8327	698-564-0557
670931411	kmarkiej	Juanita Greenwell	Armistice	Kelurahan Ciracas	Cengkareng Barat	Jakarta Barat	DKI Jakarta	Armistice	Cengkareng Barat	Kelurahan Ciracas	Jakarta Barat	DKI Jakarta	611-334-5856	713-589-6744
6111245511	mshurmerk	Frederick Hazard	Talmadge	Kelurahan Susukan	Cengkareng Timur	Jakarta Timur	DKI Jakarta	Talmadge	Cengkareng Timur	Kelurahan Susukan	Jakarta Timur	DKI Jakarta	811-648-2996	812-484-8841
9335827525	hjazel	Derrick Wenzel	Northridge	Kelurahan Rambutan  	Duri Kosambi	Jakarta Pusat	DKI Jakarta	Northridge	Duri Kosambi	Kelurahan Rambutan  	Jakarta Pusat	DKI Jakarta	680-402-4508	266-240-8240
\.


--
-- Data for Name: pengguna_publik; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.pengguna_publik (username, nik, nama, status, peran, nohp) FROM stdin;
spatemanh	7065801594	Eugen Scotland	AKTIF	Marketing Manager	81331980855
bvanni	3575135932	Sophie Booeln	TDK AKTIF	Software Consultant	81323980842
kmarkiej	3399499965	Julita Endersby	AKTIF	VP Marketing	81313980999
mshurmerk	9296170362	Cello Hartrick	TDK AKTIF	Research Nurse	81332393123
hjazel	2075727848	Benoite Apperley	AKTIF	Assistant Professor	81323983844
\.


--
-- Data for Name: reservasi_gedung; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.reservasi_gedung (kodepasien, tglmasuk, tglkeluar, kodegedung, noruang, notempattidur) FROM stdin;
1700594451	2019-05-13	2019-05-17	ged2	rng01	a1-01
9335827525	2019-04-14	2019-04-18	ged2	rng01	a1-02
7408764615	2019-05-05	2019-05-09	ged2	rng02	a2-01
2237116377	2019-03-06	2019-03-10	ged2	rng03	a3-01
124776612	2019-06-17	2019-06-21	ged3	rng01	b1-01
564032689	2019-05-10	2019-05-14	ged3	rng02	b2-01
9752161014	2019-05-19	2019-05-23	ged3	rng03	b3-01
6581815322	2019-04-20	2019-04-24	ged4	rng01	c1-01
670931411	2019-04-27	2019-05-01	ged4	rng02	c2-01
6111245511	2019-05-22	2019-05-26	ged4	rng03	c3-01
1700594451	2019-05-23	2019-05-27	ged7	rng01	d1-01
9335827525	2019-05-24	2019-05-28	ged7	rng02	d2-01
7408764615	2019-05-25	2019-05-29	ged7	rng03	d3-01
2237116377	2019-05-26	2019-05-30	ged8	rng01	e1-01
\.


--
-- Data for Name: reservasi_hotel; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.reservasi_hotel (kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom) FROM stdin;
564032689	2019-09-10	2019-09-14	ht1	hr1
9752161014	2019-08-23	2019-08-30	ht2	hr4
6581815322	2019-09-18	2019-09-29	ht3	hr7
670931411	2019-10-15	2019-10-29	ht4	hr11
6111245511	2019-08-09	2019-08-19	ht5	hr15
\.


--
-- Data for Name: reservasi_rs; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.reservasi_rs (kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed, kodeventilator) FROM stdin;
1700594451	2019-07-16	2019-07-18	fs1	rg-01	bd-01	vt-01
9335827525	2019-09-16	2019-09-17	fs2	rg-03	bd-06	vt-02
7408764615	2019-09-08	2019-09-09	fs3	rg-05	bd-12	vt-03
2237116377	2019-10-18	2019-10-25	fs4	rg-07	bd-23	vt-04
124776612	2019-08-16	2019-08-20	fs5	rg-09	bd-33	vt-05
\.


--
-- Data for Name: reservasi_rumah; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.reservasi_rumah (kodepasien, tglmasuk, tglkeluar, koderumah, kodekamar) FROM stdin;
1700594451	2019-03-14	2019-03-15	rum1	kam1
9335827525	2019-05-13	2019-05-14	rum2	kam1
7408764615	2019-09-09	2019-09-10	rum3	kam2
2237116377	2019-01-20	2019-01-21	rum1	kam2
124776612	2019-04-03	2019-04-04	rum2	kam2
564032689	2019-08-17	2019-08-18	rum5	kam1
9752161014	2019-05-02	2019-05-03	rum4	kam2
6581815322	2019-08-28	2019-08-29	rum4	kam1
\.


--
-- Data for Name: ruangan_gedung; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.ruangan_gedung (kodegedung, koderuangan, namaruangan) FROM stdin;
ged2	rng01	Mel1
ged2	rng02	Mel2
ged2	rng03	Mel3
ged3	rng01	Ap1
ged3	rng02	Ap2
ged3	rng03	Ap3
ged4	rng01	Man1
ged4	rng02	Man2
ged4	rng03	Man3
ged7	rng01	Na1
ged7	rng02	Na2
ged7	rng03	Na3
ged8	rng01	Maw1
ged8	rng02	Maw2
ged8	rng03	Maw3
\.


--
-- Data for Name: ruangan_rs; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.ruangan_rs (koders, koderuangan, tipe, jmlbed, harga) FROM stdin;
fs1	rg-01	Kelas I	4	600000
fs1	rg-02	ICU	1	1500000
fs2	rg-03	VIP	2	1150000
fs2	rg-04	Kelas I	4	600000
fs3	rg-05	Kelas III	8	170000
fs3	rg-06	VIP	2	1150000
fs4	rg-07	Kelas I	4	600000
fs4	rg-08	Kelas II	6	400000
fs5	rg-09	VIP	2	1150000
fs5	rg-10	ICU	1	1500000
\.


--
-- Data for Name: rumah; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.rumah (koderumah, nama, isrujukan, jalan, kelurahan, kecamatan, kabkot, prov) FROM stdin;
rum1	rumah saya	1	Jl. Medan Merdeka 1	Kedondong Merah	Tadika Mesra	Pacil Raya	Jawa Barat
rum2	rumah dia	1	Jl. Syarif Hidayatullah	Bukit Rendah	Gunung Rendah	Jakarta Selatan	DKI Jakarta
rum3	rumah kamu	1	Jl. Delima 3	Danau Samos	Pulo Toba	Jakarta Pusat	DKI Jakarta
rum4	rumah kita	1	Jl. Proklamasi 2	Bukit Rendah	Gunung Rendah	Jakarta Selatan	DKI Jakarta
rum5	rumah mereka	1	Jl. In Aja	Kedondong Merah	Tadika Mesra	Pacil Raya	Jawa Barat
rum6	rumah kalian	1	Jl. Menuju Kemenangan	Ju Ju	Sukamanjur	Cicakdidinding	Jawa Barat
rum7	rumah siapa	0	Jl. Groove	Ikatan Kaki Lima	Cap Kaki Tiga	Cikaki	Jawa Barat
rum8	rumah sehat	0	Jl. Baker	Danau Samos	Pulo Toba	Jakarta Pusat	DKI Jakarta
rum9	rumah mainan	0	Jl. Brook	Bukit Rendah	Gunung Rendah	Jakarta Selatan	DKI Jakarta
rum10	rumah belajar	0	Jl. Cempaka Putih	Ju Ju	Sukamanjur	Cicakdidinding	Jawa Barat
\.


--
-- Data for Name: rumah_sakit; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.rumah_sakit (kode_faskes, isrujukan) FROM stdin;
fs1	1
fs2	1
fs3	0
fs4	1
fs5	0
\.


--
-- Data for Name: telepon_faskes; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.telepon_faskes (kode_faskes, notelp) FROM stdin;
fs1	8146440025
fs1	8129208022
fs2	8124508678
fs3	8103846396
fs3	8182255230
fs4	8183794188
fs5	8147654357
\.


--
-- Data for Name: tempat_tidur_gedung; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.tempat_tidur_gedung (kodegedung, koderuangan, notempattidur) FROM stdin;
ged2	rng01	a1-01
ged2	rng01	a1-02
ged2	rng02	a2-01
ged2	rng03	a3-01
ged3	rng01	b1-01
ged3	rng02	b2-01
ged3	rng03	b3-01
ged4	rng01	c1-01
ged4	rng02	c2-01
ged4	rng03	c3-01
ged7	rng01	d1-01
ged7	rng02	d2-01
ged7	rng03	d3-01
ged8	rng01	e1-01
ged8	rng02	e2-01
ged8	rng03	e3-01
\.


--
-- Data for Name: tes; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.tes (nik_pasien, tanggaltes, jenis, status, nilaict) FROM stdin;
124776612	2019-02-02	Antigen	Non-Reaktif	9.00
564032689	2019-06-10	PCR	Non-Reaktif	8.44
7408764615	2019-08-13	Antigen	Non-Reaktif	9.01
1700594451	2019-08-06	PCR	Non-Reaktif	8.45
9752161014	2019-09-07	Antigen	Reaktif	9.02
6581815322	2019-09-08	PCR	Reaktif	8.46
2237116377	2019-10-17	Antigen	Reaktif	9.03
670931411	2019-10-12	PCR	Reaktif	8.47
6111245511	2019-12-18	Antigen	Non-Reaktif	9.04
9335827525	2019-09-08	PCR	Non-Reaktif	8.48
\.


--
-- Data for Name: transaksi_booking; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.transaksi_booking (idtransaksibooking, totalbayar, kodepasien, tglmasuk) FROM stdin;
trh1	600000	564032689	2019-09-10
trh2	1050000	9752161014	2019-08-23
trh3	4950000	6581815322	2019-09-18
trh4	6300000	670931411	2019-10-15
trh5	7000000	6111245511	2019-08-09
\.


--
-- Data for Name: transaksi_hotel; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.transaksi_hotel (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, totalbayar, statusbayar) FROM stdin;
trh1	564032689	2019-09-14	2019-09-14 13:30:00	800000	Lunas
trh2	9752161014	2019-08-30	2019-08-30 14:30:00	1270000	Lunas
trh3	6581815322	2019-09-29	2019-09-29 13:30:00	5185000	Belum Lunas
trh4	670931411	2019-10-31	2019-10-31 12:00:00	6605000	Lunas
trh5	6111245511	2019-08-19	2019-08-19 09:15:00	7230000	Belum Lunas
\.


--
-- Data for Name: transaksi_makan; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.transaksi_makan (idtransaksi, idtransaksimakan, totalbayar) FROM stdin;
trh1	trm1	145000
trh2	trm2	165000
trh3	trm3	180000
trh4	trm4	250000
trh5	trm5	175000
\.


--
-- Data for Name: transaksi_rs; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.transaksi_rs (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, tglmasuk, totalbiaya, statusbayar) FROM stdin;
trs-1	1700594451	2019-07-18	2019-07-18 13:10:00	2019-07-16	1200000	Lunas
trs-2	9335827525	2019-09-17	2019-09-17 12:10:00	2019-09-16	1150000	Lunas
trs-3	7408764615	2019-09-09	2019-09-09 15:30:00	2019-09-08	170000	Lunas
trs-4	2237116377	2019-10-25	2019-10-25 12:50:00	2019-10-18	4200000	Belum Lunas
trs-5	124776612	2019-08-20	2019-08-20 07:40:00	2019-08-16	5750000	Belum Lunas
\.


--
-- Data for Name: transaksi_rumah; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.transaksi_rumah (idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, totalbayar, statusbayar, tglmasuk) FROM stdin;
TR0001	1700594451	2019-03-15	2019-03-15 05:00:00	600000	Belum Dibayar	2019-03-14
TR0002	9335827525	2019-05-14	2019-05-14 06:00:00	600000	Lunas	2019-05-13
TR0003	7408764615	2019-09-10	2019-09-10 12:00:00	900000	Lunas	2019-09-09
TR0004	2237116377	2019-01-21	2019-01-21 17:00:00	90000	Lunas	2019-01-20
TR0005	124776612	2019-04-04	2019-04-04 05:30:00	900000	Cicilan	2019-04-03
TR0006	564032689	2019-08-18	2019-08-18 06:50:00	600000	Lunas	2019-08-17
TR0007	9752161014	2019-05-03	2019-05-03 09:30:00	900000	Lunas	2019-05-02
TR0008	6581815322	2019-08-29	2019-08-29 10:00:00	6000000	Lunas	2019-08-28
\.


--
-- Data for Name: ventilator; Type: TABLE DATA; Schema: siruco; Owner: db202c08
--

COPY siruco.ventilator (koders, kodeventilator, kondisi) FROM stdin;
fs1	vt-01	baik
fs2	vt-02	baik
fs3	vt-03	perbaikan
fs4	vt-04	rusak
fs5	vt-05	baik
\.


--
-- Name: daftar_pesan_id_pesanan_seq; Type: SEQUENCE SET; Schema: siruco; Owner: db202c08
--

SELECT pg_catalog.setval('siruco.daftar_pesan_id_pesanan_seq', 36, true);


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (username);


--
-- Name: admin_satgas admin_satgas_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.admin_satgas
    ADD CONSTRAINT admin_satgas_pkey PRIMARY KEY (username);


--
-- Name: akun_pengguna akun_pengguna_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.akun_pengguna
    ADD CONSTRAINT akun_pengguna_pkey PRIMARY KEY (username);


--
-- Name: bed_rs bed_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.bed_rs
    ADD CONSTRAINT bed_rs_pkey PRIMARY KEY (koderuangan, koders, kodebed);


--
-- Name: daftar_pesan daftar_pesan_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.daftar_pesan
    ADD CONSTRAINT daftar_pesan_pkey PRIMARY KEY (idtransaksimakan, id_pesanan, id_transaksi);


--
-- Name: dokter dokter_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.dokter
    ADD CONSTRAINT dokter_pkey PRIMARY KEY (username, nostr);


--
-- Name: faskes faskes_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.faskes
    ADD CONSTRAINT faskes_pkey PRIMARY KEY (kode);


--
-- Name: gedung gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.gedung
    ADD CONSTRAINT gedung_pkey PRIMARY KEY (kodegedung);


--
-- Name: gejala_pasien gejala_pasien_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.gejala_pasien
    ADD CONSTRAINT gejala_pasien_pkey PRIMARY KEY (nik, namagejala);


--
-- Name: hotel hotel_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.hotel
    ADD CONSTRAINT hotel_pkey PRIMARY KEY (kode);


--
-- Name: hotel_room hotel_room_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.hotel_room
    ADD CONSTRAINT hotel_room_pkey PRIMARY KEY (kodehotel, koderoom);


--
-- Name: jadwal_dokter jadwal_dokter_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.jadwal_dokter
    ADD CONSTRAINT jadwal_dokter_pkey PRIMARY KEY (nostr, username, kode_faskes, shift, tanggal);


--
-- Name: jadwal jadwal_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.jadwal
    ADD CONSTRAINT jadwal_pkey PRIMARY KEY (kode_faskes, shift, tanggal);


--
-- Name: kamar_rumah kamar_rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.kamar_rumah
    ADD CONSTRAINT kamar_rumah_pkey PRIMARY KEY (koderumah, kodekamar);


--
-- Name: komorbid_pasien komorbid_pasien_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.komorbid_pasien
    ADD CONSTRAINT komorbid_pasien_pkey PRIMARY KEY (nik, namakomorbid);


--
-- Name: memeriksa memeriksa_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.memeriksa
    ADD CONSTRAINT memeriksa_pkey PRIMARY KEY (nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl);


--
-- Name: paket_makan paket_makan_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.paket_makan
    ADD CONSTRAINT paket_makan_pkey PRIMARY KEY (kodehotel, kodepaket);


--
-- Name: pasien pasien_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.pasien
    ADD CONSTRAINT pasien_pkey PRIMARY KEY (nik);


--
-- Name: pengguna_publik pengguna_publik_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.pengguna_publik
    ADD CONSTRAINT pengguna_publik_pkey PRIMARY KEY (username);


--
-- Name: reservasi_gedung reservasi_gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_gedung
    ADD CONSTRAINT reservasi_gedung_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: reservasi_hotel reservasi_hotel_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_hotel
    ADD CONSTRAINT reservasi_hotel_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: reservasi_rs reservasi_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: reservasi_rumah reservasi_rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_rumah
    ADD CONSTRAINT reservasi_rumah_pkey PRIMARY KEY (kodepasien, tglmasuk);


--
-- Name: ruangan_gedung ruangan_gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.ruangan_gedung
    ADD CONSTRAINT ruangan_gedung_pkey PRIMARY KEY (kodegedung, koderuangan);


--
-- Name: ruangan_rs ruangan_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.ruangan_rs
    ADD CONSTRAINT ruangan_rs_pkey PRIMARY KEY (koders, koderuangan);


--
-- Name: rumah rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.rumah
    ADD CONSTRAINT rumah_pkey PRIMARY KEY (koderumah);


--
-- Name: rumah_sakit rumah_sakit_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.rumah_sakit
    ADD CONSTRAINT rumah_sakit_pkey PRIMARY KEY (kode_faskes);


--
-- Name: telepon_faskes telepon_faskes_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.telepon_faskes
    ADD CONSTRAINT telepon_faskes_pkey PRIMARY KEY (kode_faskes, notelp);


--
-- Name: tempat_tidur_gedung tempat_tidur_gedung_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.tempat_tidur_gedung
    ADD CONSTRAINT tempat_tidur_gedung_pkey PRIMARY KEY (kodegedung, koderuangan, notempattidur);


--
-- Name: tes tes_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.tes
    ADD CONSTRAINT tes_pkey PRIMARY KEY (nik_pasien, tanggaltes);


--
-- Name: transaksi_booking transaksi_booking_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_pkey PRIMARY KEY (idtransaksibooking);


--
-- Name: transaksi_hotel transaksi_hotel_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_hotel
    ADD CONSTRAINT transaksi_hotel_pkey PRIMARY KEY (idtransaksi);


--
-- Name: transaksi_makan transaksi_makan_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_makan
    ADD CONSTRAINT transaksi_makan_pkey PRIMARY KEY (idtransaksi, idtransaksimakan);


--
-- Name: transaksi_rs transaksi_rs_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_rs
    ADD CONSTRAINT transaksi_rs_pkey PRIMARY KEY (idtransaksi);


--
-- Name: transaksi_rumah transaksi_rumah_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_rumah
    ADD CONSTRAINT transaksi_rumah_pkey PRIMARY KEY (idtransaksi);


--
-- Name: ventilator ventilator_pkey; Type: CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.ventilator
    ADD CONSTRAINT ventilator_pkey PRIMARY KEY (koders, kodeventilator);


--
-- Name: transaksi_hotel add_trbooking; Type: TRIGGER; Schema: siruco; Owner: db202c08
--

CREATE TRIGGER add_trbooking AFTER INSERT ON siruco.transaksi_hotel FOR EACH ROW EXECUTE PROCEDURE siruco.add_transaksi_booking();


--
-- Name: reservasi_hotel add_trhotel; Type: TRIGGER; Schema: siruco; Owner: db202c08
--

CREATE TRIGGER add_trhotel AFTER INSERT ON siruco.reservasi_hotel FOR EACH ROW EXECUTE PROCEDURE siruco.add_transaksi_hotel();


--
-- Name: daftar_pesan periksa_paket_makan; Type: TRIGGER; Schema: siruco; Owner: db202c08
--

CREATE TRIGGER periksa_paket_makan BEFORE INSERT ON siruco.daftar_pesan FOR EACH ROW EXECUTE PROCEDURE siruco.add_daftar_pesan();


--
-- Name: reservasi_rs transaksi_reservasi_rs; Type: TRIGGER; Schema: siruco; Owner: db202c08
--

CREATE TRIGGER transaksi_reservasi_rs AFTER INSERT ON siruco.reservasi_rs FOR EACH ROW EXECUTE PROCEDURE siruco.create_transaksi_rs();


--
-- Name: akun_pengguna trigger_password; Type: TRIGGER; Schema: siruco; Owner: db202c08
--

CREATE TRIGGER trigger_password BEFORE INSERT OR UPDATE ON siruco.akun_pengguna FOR EACH ROW EXECUTE PROCEDURE siruco.cek_password();


--
-- Name: bed_rs validasi_memeriksa; Type: TRIGGER; Schema: siruco; Owner: db202c08
--

CREATE TRIGGER validasi_memeriksa AFTER INSERT OR DELETE ON siruco.bed_rs FOR EACH ROW EXECUTE PROCEDURE siruco.nambah_bed();


--
-- Name: memeriksa validasi_memeriksa; Type: TRIGGER; Schema: siruco; Owner: db202c08
--

CREATE TRIGGER validasi_memeriksa BEFORE INSERT ON siruco.memeriksa FOR EACH ROW EXECUTE PROCEDURE siruco.cek_jumlah_pasien();


--
-- Name: reservasi_rs validasi_memeriksa2; Type: TRIGGER; Schema: siruco; Owner: db202c08
--

CREATE TRIGGER validasi_memeriksa2 AFTER INSERT OR DELETE ON siruco.reservasi_rs FOR EACH ROW EXECUTE PROCEDURE siruco.pengurangan_bed();


--
-- Name: admin_satgas admin_satgas_idfaskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.admin_satgas
    ADD CONSTRAINT admin_satgas_idfaskes_fkey FOREIGN KEY (idfaskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin_satgas admin_satgas_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.admin_satgas
    ADD CONSTRAINT admin_satgas_username_fkey FOREIGN KEY (username) REFERENCES siruco.admin(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin admin_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.admin
    ADD CONSTRAINT admin_username_fkey FOREIGN KEY (username) REFERENCES siruco.akun_pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: bed_rs bed_rs_koderuangan_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.bed_rs
    ADD CONSTRAINT bed_rs_koderuangan_fkey FOREIGN KEY (koders, koderuangan) REFERENCES siruco.ruangan_rs(koders, koderuangan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: daftar_pesan daftar_pesan_id_transaksi_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.daftar_pesan
    ADD CONSTRAINT daftar_pesan_id_transaksi_fkey FOREIGN KEY (id_transaksi, idtransaksimakan) REFERENCES siruco.transaksi_makan(idtransaksi, idtransaksimakan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: daftar_pesan daftar_pesan_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.daftar_pesan
    ADD CONSTRAINT daftar_pesan_kodehotel_fkey FOREIGN KEY (kodehotel, kodepaket) REFERENCES siruco.paket_makan(kodehotel, kodepaket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: dokter dokter_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.dokter
    ADD CONSTRAINT dokter_username_fkey FOREIGN KEY (username) REFERENCES siruco.admin(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: gejala_pasien gejala_pasien_nik_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.gejala_pasien
    ADD CONSTRAINT gejala_pasien_nik_fkey FOREIGN KEY (nik) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: hotel_room hotel_room_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.hotel_room
    ADD CONSTRAINT hotel_room_kodehotel_fkey FOREIGN KEY (kodehotel) REFERENCES siruco.hotel(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: jadwal_dokter jadwal_dokter_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.jadwal_dokter
    ADD CONSTRAINT jadwal_dokter_kode_faskes_fkey FOREIGN KEY (kode_faskes, shift, tanggal) REFERENCES siruco.jadwal(kode_faskes, shift, tanggal) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: jadwal_dokter jadwal_dokter_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.jadwal_dokter
    ADD CONSTRAINT jadwal_dokter_username_fkey FOREIGN KEY (username, nostr) REFERENCES siruco.dokter(username, nostr) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: jadwal jadwal_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.jadwal
    ADD CONSTRAINT jadwal_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kamar_rumah kamar_rumah_koderumah_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.kamar_rumah
    ADD CONSTRAINT kamar_rumah_koderumah_fkey FOREIGN KEY (koderumah) REFERENCES siruco.rumah(koderumah) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: komorbid_pasien komorbid_pasien_nik_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.komorbid_pasien
    ADD CONSTRAINT komorbid_pasien_nik_fkey FOREIGN KEY (nik) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: memeriksa memeriksa_nik_pasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.memeriksa
    ADD CONSTRAINT memeriksa_nik_pasien_fkey FOREIGN KEY (nik_pasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: memeriksa memeriksa_nostr_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.memeriksa
    ADD CONSTRAINT memeriksa_nostr_fkey FOREIGN KEY (nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl) REFERENCES siruco.jadwal_dokter(nostr, username, kode_faskes, shift, tanggal) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: paket_makan paket_makan_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.paket_makan
    ADD CONSTRAINT paket_makan_kodehotel_fkey FOREIGN KEY (kodehotel) REFERENCES siruco.hotel(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pasien pasien_idpendaftar_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.pasien
    ADD CONSTRAINT pasien_idpendaftar_fkey FOREIGN KEY (idpendaftar) REFERENCES siruco.pengguna_publik(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengguna_publik pengguna_publik_username_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.pengguna_publik
    ADD CONSTRAINT pengguna_publik_username_fkey FOREIGN KEY (username) REFERENCES siruco.akun_pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_gedung reservasi_gedung_kodegedung_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_gedung
    ADD CONSTRAINT reservasi_gedung_kodegedung_fkey FOREIGN KEY (kodegedung, noruang, notempattidur) REFERENCES siruco.tempat_tidur_gedung(kodegedung, koderuangan, notempattidur) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_gedung reservasi_gedung_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_gedung
    ADD CONSTRAINT reservasi_gedung_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_hotel reservasi_hotel_kodehotel_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_hotel
    ADD CONSTRAINT reservasi_hotel_kodehotel_fkey FOREIGN KEY (kodehotel, koderoom) REFERENCES siruco.hotel_room(kodehotel, koderoom) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_hotel reservasi_hotel_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_hotel
    ADD CONSTRAINT reservasi_hotel_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rs reservasi_rs_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rs reservasi_rs_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koders_fkey FOREIGN KEY (koders, kodeventilator) REFERENCES siruco.ventilator(koders, kodeventilator) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rs reservasi_rs_koderuangan_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_rs
    ADD CONSTRAINT reservasi_rs_koderuangan_fkey FOREIGN KEY (koderuangan, koders, kodebed) REFERENCES siruco.bed_rs(koderuangan, koders, kodebed) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rumah reservasi_rumah_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_rumah
    ADD CONSTRAINT reservasi_rumah_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservasi_rumah reservasi_rumah_koderumah_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.reservasi_rumah
    ADD CONSTRAINT reservasi_rumah_koderumah_fkey FOREIGN KEY (koderumah, kodekamar) REFERENCES siruco.kamar_rumah(koderumah, kodekamar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ruangan_gedung ruangan_gedung_kodegedung_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.ruangan_gedung
    ADD CONSTRAINT ruangan_gedung_kodegedung_fkey FOREIGN KEY (kodegedung) REFERENCES siruco.gedung(kodegedung) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ruangan_rs ruangan_rs_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.ruangan_rs
    ADD CONSTRAINT ruangan_rs_koders_fkey FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rumah_sakit rumah_sakit_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.rumah_sakit
    ADD CONSTRAINT rumah_sakit_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: telepon_faskes telepon_faskes_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.telepon_faskes
    ADD CONSTRAINT telepon_faskes_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES siruco.faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tempat_tidur_gedung tempat_tidur_gedung_kodegedung_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.tempat_tidur_gedung
    ADD CONSTRAINT tempat_tidur_gedung_kodegedung_fkey FOREIGN KEY (kodegedung, koderuangan) REFERENCES siruco.ruangan_gedung(kodegedung, koderuangan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tes tes_nik_pasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.tes
    ADD CONSTRAINT tes_nik_pasien_fkey FOREIGN KEY (nik_pasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_booking transaksi_booking_idtransaksibooking_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_idtransaksibooking_fkey FOREIGN KEY (idtransaksibooking) REFERENCES siruco.transaksi_hotel(idtransaksi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_booking transaksi_booking_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_booking transaksi_booking_kodepasien_fkey1; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_booking
    ADD CONSTRAINT transaksi_booking_kodepasien_fkey1 FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_hotel(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_hotel transaksi_hotel_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_hotel
    ADD CONSTRAINT transaksi_hotel_kodepasien_fkey FOREIGN KEY (kodepasien) REFERENCES siruco.pasien(nik) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_makan transaksi_makan_idtransaksi_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_makan
    ADD CONSTRAINT transaksi_makan_idtransaksi_fkey FOREIGN KEY (idtransaksi) REFERENCES siruco.transaksi_hotel(idtransaksi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_rs transaksi_rs_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_rs
    ADD CONSTRAINT transaksi_rs_kodepasien_fkey FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_rs(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_rumah transaksi_rumah_kodepasien_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.transaksi_rumah
    ADD CONSTRAINT transaksi_rumah_kodepasien_fkey FOREIGN KEY (kodepasien, tglmasuk) REFERENCES siruco.reservasi_rumah(kodepasien, tglmasuk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ventilator ventilator_koders_fkey; Type: FK CONSTRAINT; Schema: siruco; Owner: db202c08
--

ALTER TABLE ONLY siruco.ventilator
    ADD CONSTRAINT ventilator_koders_fkey FOREIGN KEY (koders) REFERENCES siruco.rumah_sakit(kode_faskes) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

