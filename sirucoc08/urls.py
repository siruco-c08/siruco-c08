"""sirucoc08 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls'),name="main"),
    path('jadwal/', include('jadwal.urls'),name="jadwal"),
    path('user/', include('user.urls'),name="user"),
    path('profil/', include('profil.urls'),name="profil"),
    path('ruanghotel/', include('ruanganhotel.urls'),name="ruanganhotel"),
    path('appoinment/', include('appoinment.urls'),name="appoinment"),
    path('ruanganrs/', include('ruanganrs.urls'),name="ruanganrs"),
    path('faskes/', include('faskes.urls'),name="faskes"),
    path('jadwaldokter/', include('jadwaldokter.urls'),name="jadwaldokter"),
    path('reservasirs/', include('reservasirumahsakit.urls'),name="reservasirs"),
    path('transaksirs/', include('transaksirumahsakit.urls'),name="transaksirs"),
    path('rumahsakit/', include('rumahsakit.urls'),name="rumahsakit"),
    path('reservasihotel/', include('reservasihotel.urls'),name="reservasihotel"),
    path('transaksihotel/', include('transaksihotel.urls'),name="transaksihotel"),
    path('transaksibooking/', include('transaksibooking.urls'),name="transaksibooking"),
    path('pasien/', include('pasien.urls'),name="pasien"),
    path('hotel/', include('hotel.urls'), name="hotel"),
    path('transaksimakan/', include('transaksimakan.urls'), name="transaksimakan"),
]


