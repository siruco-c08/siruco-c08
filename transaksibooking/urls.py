from django.urls import path

from . import views

app_name = 'transaksibooking'

urlpatterns = [
    path('', views.read_transaksi_booking, name='read_transaksi_booking'),
]
