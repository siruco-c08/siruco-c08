from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse


def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def read_transaksi_booking(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from siruco.transaksi_booking")
        data = fetch(cursor)
        return render(request, 'listtransaksibooking.html', {'data':data})
    elif (role == 'Pengguna Publik'):
        cursor = connection.cursor()
        username = request.session['email']
        cursor.execute("select * from siruco.transaksi_booking, siruco.pasien where kodepasien = nik and idpendaftar = '{}' ".format(username))
        data = fetch(cursor)
        return render(request, 'listtransaksibooking.html', {'data':data})
    else:
        return HttpResponseRedirect('/profil/profil')