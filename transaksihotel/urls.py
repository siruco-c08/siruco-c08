from django.urls import path

from . import views

app_name = 'transaksihotel'

urlpatterns = [
    path('', views.read_transaksi_hotel, name='read_transaksi_hotel'),
    path('update/<str:idtransaksi>', views.update_transaksi_hotel, name='update_transaksi_hotel'),
]
