from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *


def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def read_transaksi_hotel(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran::time, totalbayar, statusbayar  from siruco.transaksi_hotel")
        data = fetch(cursor)
        return render(request, 'listtransaksi.html', {'data':data})
    else:
        return HttpResponseRedirect('/profil/profil')
    
def update_transaksi_hotel(request, idtransaksi):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        with connection.cursor() as c:
            c.execute("select idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran::time, totalbayar, statusbayar FROM siruco.transaksi_hotel WHERE idtransaksi = '{}' ".format(idtransaksi))
            data = fetch(c)
        initial_data = {
            'idtransaksi' : data[0]['idtransaksi'],
            'kodepasien' : data[0]['kodepasien'],
            'tanggalpembayaran' : data[0]['tanggalpembayaran'],
            'waktupembayaran' : data[0]['waktupembayaran'],
            'totalbayar' : data[0]['totalbayar'],
            'statusbayar' : data [0]['statusbayar']
        }
        if request.method == 'POST':
            form = updatetransaksi(request.POST)
            if form.is_valid():
                statusbayar = request.POST.get('statusbayar')
                c = connection.cursor()
                c.execute("UPDATE siruco.transaksi_hotel SET statusbayar = '{}' where idtransaksi = '{}' ;".format(statusbayar, idtransaksi))
                return HttpResponseRedirect(reverse('transaksihotel:read_transaksi_hotel'))

        update = updatetransaksi(initial = initial_data)
        response = {'form':update, 'lama':initial_data}
        return render(request,'updatetransaksi.html',response)
    else:
        return HttpResponseRedirect('/profil/profil')


