from django.apps import AppConfig


class TransaksimakanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transaksimakan'
