from django.urls import path

from . import views

app_name = 'transaksimakan'

urlpatterns = [
    path('read/', views.transaksi_makan_read, name='transaksi_makan_read'),
    path('create/', views.transaksi_makan_create, name='transaksi_makan_read'),
    path('update/', views.transaksi_makan_update, name='transaksi_makan_update'),
    path('delete/', views.transaksi_makan_delete, name='transaksi_makan_delete'),
]
