from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *


def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def transaksi_makan_read(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == "admin satgas" or role == "Pengguna Publik"):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.daftar_pesan")
        data_transaksi = fetch(cursor)
        return render(request, 'transaksimakanlst.html', {'data_transaksi':data_transaksi})

def transaksi_makan_create(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    if (role == "admin satgas" or role == "Pengguna Publik"):

        return render(request, 'transaksimakancrt.html')

def transaksi_makan_update(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    if (role == "admin satgas"):


        return render(request,'transaksimakanupd.html')
    else:
        return HttpResponseRedirect('/profil/profil')

def transaksi_makan_delete(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    if (role == "admin satgas"):

        return render(request,'transaksimakandlt.html')
    else:
        return HttpResponseRedirect('/profil/profil')