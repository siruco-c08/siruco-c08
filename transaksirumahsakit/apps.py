from django.apps import AppConfig


class TransaksirumahsakitConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transaksirumahsakit'
