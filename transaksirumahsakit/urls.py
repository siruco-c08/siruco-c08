from django.urls import path

from . import views

app_name = 'transaksirumahsakit'

urlpatterns = [
    path('', views.transaksirs_lihat, name='transaksirs_lihat'),
    path('update/<str:idtransaksi>', views.transaksirs_update, name='transaksirs_update'),
    path('hapus/<str:idtransaksi>', views.transaksirs_hapus, name='transaksirs_hapus'),
]
