from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect, response, JsonResponse
from .forms import *

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def transaksirs_lihat(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        cursor = connection.cursor()
        cursor.execute("select * from SIRUCO.transaksi_rs")
        data_transaksirs = fetch(cursor)
        return render(request, 'transaksirs.html', {'data_transaksirs':data_transaksirs, 'role':role})

    else:
        return HttpResponseRedirect('/user/login')

def transaksirs_update(request, idtransaksi):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        with connection.cursor() as c:
            c.execute("select idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran::time, tglmasuk, totalbiaya, statusbayar FROM SIRUCO.transaksi_rs WHERE idtransaksi = '{}' ".format(idtransaksi))
            data = fetch(c)
        initial_data = {
            'idtransaksi' : data[0]['idtransaksi'],
            'kodepasien' : data[0]['kodepasien'],
            'tanggalpembayaran' : data[0]['tanggalpembayaran'],
            'waktupembayaran' : data[0]['waktupembayaran'],
            'tglmasuk' : data[0]['tglmasuk'],
            'totalbiaya' : data[0]['totalbiaya'],
            'statusbayar' : data [0]['statusbayar']
        }
        if request.method == 'POST':
            form = updatetransaksirs(request.POST)
            if form.is_valid():
                statusbayar = request.POST.get('statusbayar')
                c = connection.cursor()
                c.execute("UPDATE SIRUCO.transaksi_rs SET statusbayar = '{}' where idtransaksi = '{}' ;".format(statusbayar, idtransaksi))
                return HttpResponseRedirect(reverse('transaksirumahsakit:transaksirs_lihat'))

        update = updatetransaksirs(initial = initial_data)
        response = {'form':update, 'lama':initial_data}
        return render(request,'updatetransaksirs.html',response)
    else:
        return HttpResponseRedirect('/profil/profil')

def transaksirs_hapus(request, idtransaksi):
    if 'email' not in request.session:
        return HttpResponseRedirect('/user/login')
    role = request.session['role']
    if (role == 'admin satgas'):
        with connection.cursor() as c:
            c.execute("delete from SIRUCO.transaksi_rs where idtransaksi= '{}'".format(idtransaksi))
        return HttpResponseRedirect(reverse('transaksirumahsakit:transaksirs_lihat'))
    else:
        return HttpResponseRedirect(reverse('transaksirumahsakit:transaksirs_lihat'))