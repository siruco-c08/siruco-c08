from django.urls import path

from . import views

app_name = 'user'

urlpatterns = [
    path('login/', views.login, name='login'),
    path('regist/', views.regist, name='regist'),
    path('logout/', views.logout, name='logout'),
    path('regist/sistem/', views.regist_sistem, name='sistem'),
    path('regist/dokter', views.regist_dokter, name='dokter'),
    path('regist/pengguna', views.regist_pengguna, name='pengguna'),
    path('regist/satgas', views.regist_satgas, name='satgas'),
]
