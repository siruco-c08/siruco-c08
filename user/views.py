from django.shortcuts import render,redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from .forms import loginform, sistemform, penggunaform, dokterform, satgasform
from django.urls import reverse


def login(request):
	form = loginform()
	if 'email' in request.session:
		return redirect(reverse('profil:profil'))
	if request.method == 'POST':
		form = loginform(request.POST)
		if form.is_valid():
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			if check_user_is_exist("AKUN_PENGGUNA",email,password):
				request.session['email'] = email
				request.session['password'] = password
				role = cekrole(email)
				request.session['role'] = role
				return redirect('/profil/profil')
			else:
				messages.error(request, 'Email/Password salah')
				return redirect(reverse('user:login'))
	form = loginform()
	response = {'form':form}
	return render(request,'main/login.html',response)

def cekrole(username):
	data = {}
	c = connection.cursor()
	c.execute("SELECT peran FROM SIRUCO.AKUN_PENGGUNA where username = '{}'".format(username))
	data = fetch(c)[0]['peran']
	print(data)
	return data



def check_user_is_exist(table, email, password = None):
	data = {}
	c = connection.cursor()
	if password is not None :
		c.execute("SELECT * FROM SIRUCO.{} where username = '{}' and password = '{}' ".format(table,email,password))
		data = fetch(c)
	else:
		c.execute("SELECT * FROM SIRUCO.{} where username = '{}' ".format(table,email))
		data = fetch(c)
	
	return len(data)>0


def logout(request):
    request.session.flush()
    request.session.clear_expired()
    return redirect('/')

def regist(request):
	return render(request, 'main/regist.html')
def regist_sistem(request):
	cursor = connection.cursor()
	if request.method == 'POST':
		form = sistemform(request.POST)
		try:
			if form.is_valid():
				username = request.POST.get('username')
				password = request.POST.get('password')
				cursor.execute("INSERT INTO siruco.akun_pengguna(username, password, peran)"
					"VALUES ('{}', '{}', '{}');".format(username,password,"Admin"))
				cursor.execute("INSERT INTO siruco.admin(username)"
					"VALUES ('{}');".format(username))
				request.session['email'] = username
				request.session['password'] = password
				request.session['role'] = "Admin"

				return redirect('/')
		except:
			messages.error(request, 'Password harus mengandung minimal 1 angka dan 1 huruf kapital')
			return redirect(reverse('user:sistem'))
	cursor.execute("select * from siruco.akun_pengguna;")
	data_pengguna = fetch(cursor)
	form = sistemform()
	args = {
		'form' : form,
		'data' : data_pengguna
	}
	return render(request,'main/sistem.html',args)

def regist_dokter(request):
	cursor = connection.cursor()
	if request.method == 'POST':
		form = dokterform(request.POST)
		try:
			if form.is_valid():
				username = request.POST.get('username')
				password = request.POST.get('password')
				noSTR = request.POST.get('noSTR')
				nama = request.POST.get('nama')
				noHP = request.POST.get('noHP')
				GelarDepan = request.POST.get('GelarDepan')
				GelarBelakang = request.POST.get('GelarBelakang')
				cursor.execute("INSERT INTO siruco.akun_pengguna(username, password, peran)"
					"VALUES ('{}', '{}', '{}');".format(username,password,"dokter"))
				cursor.execute("INSERT INTO siruco.admin(username)"
					"VALUES ('{}');".format(username))
				cursor.execute("INSERT INTO siruco.dokter(username,nostr,nama,nohp,gelardepan,gelarbelakang)"
					"VALUES ('{}', '{}', '{}', '{}', '{}', '{}');".format(username,noSTR, nama, noHP, GelarDepan, GelarBelakang))
				request.session['email'] = username
				request.session['password'] = password
				request.session['role'] = "dokter"
				return redirect('/')
		except:
			messages.error(request, 'Password harus mengandung minimal 1 angka dan 1 huruf kapital')
			return redirect(reverse('user:dokter'))
	cursor.execute("select * from siruco.dokter;")
	data_pengguna = fetch(cursor)
	form = dokterform()
	args = {
		'form' : form,
		'data' : data_pengguna
	}
	return render(request,'main/dokter.html',args)

def regist_pengguna(request):
	cursor = connection.cursor()
	if request.method == 'POST':
		form = penggunaform(request.POST)
		try: 
			if form.is_valid():
				username = request.POST.get('email')
				password = request.POST.get('password')
				nama = request.POST.get('nama')
				nik = request.POST.get('nik')
				no_hp = request.POST.get('no_hp')
				cursor.execute("INSERT INTO siruco.akun_pengguna(username, password, peran)"
					"VALUES ('{}', '{}', '{}');".format(username,password,"Pengguna Publik"))
				cursor.execute("INSERT INTO siruco.pengguna_publik(username, nik, nama, status, peran, nohp)"
					"VALUES ('{}', '{}','{}','{}','{}','{}' );".format(username, nik, nama,"aktif", "pengguna", no_hp))
				request.session['email'] = username
				request.session['password'] = password
				request.session['role'] = "Pengguna Publik"
				return redirect('/')
		except:
			messages.error(request, 'Password harus mengandung minimal 1 angka dan 1 huruf kapital')
			return redirect(reverse('user:pengguna'))
		
	cursor.execute("select * from siruco.pengguna_publik;")
	form = penggunaform()
	args = {
		'form' : form
	}
	return render(request, 'main/pengguna.html',args)

def regist_satgas(request):
	cursor = connection.cursor()
	if request.method == 'POST':
		form= satgasform(request.POST)
		try:
			if form.is_valid():
				username = request.POST.get('username')
				password = request.POST.get('password')
				kodefaskes= request.POST.get('Kode')
				cursor.execute("INSERT INTO siruco.akun_pengguna(username, password, peran)"
					"VALUES ('{}', '{}', '{}');".format(username,password,"admin satgas"))
				cursor.execute("INSERT INTO siruco.admin(username)"
					"VALUES ('{}');".format(username))
				cursor.execute("INSERT INTO siruco.admin_satgas(username,idfaskes)"
					"VALUES ('{}', '{}');".format(username, kodefaskes))
				request.session['email'] = username
				request.session['password'] = password
				request.session['role'] = "admin satgas"
				return redirect('/')
		except:
			messages.error(request, 'Password harus mengandung minimal 1 angka dan 1 huruf kapital')
			return redirect(reverse('user:satgas'))
	cursor.execute("select kode from siruco.faskes;")

	faskes = fetch(cursor)
	cursor.execute("select * from siruco.admin_satgas;")
	data=fetch(cursor)
	form = satgasform()
	args = {
		'form' : form,
		'data' : faskes,
		'admins': data
	}
	return render(request,'main/satgas.html',args)

def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
	columns = [column[0] for column in cursor.description]
	results = []
	for row in cursor.fetchall():
		results.append(dict(zip(columns, row)))
	return results

